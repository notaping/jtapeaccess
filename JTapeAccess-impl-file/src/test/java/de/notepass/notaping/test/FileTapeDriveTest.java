package de.notepass.notaping.test;

import de.notepass.notaping.core.access.jTapeAccess.impl.file.FileBasedTapeDrive;
import de.notepass.notaping.core.fs.etfs.action.impl.AcceptAllFileSelector;
import de.notepass.notaping.core.fs.etfs.action.impl.NoopTapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.action.model.FileSelector;
import de.notepass.notaping.core.fs.etfs.action.model.PathRemapper;
import de.notepass.notaping.core.fs.etfs.data.listener.impl.EtfsNoopReadProgressListener;
import de.notepass.notaping.core.fs.etfs.data.listener.impl.EtfsNoopWriteProgressListener;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsReadDirectionBuilder;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsWriteDirectionBuilder;
import org.junit.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class FileTapeDriveTest {
    // References to input files
    static List<Path> inputFiles = new ArrayList<>();

    // Folder the restored files fill be stored in
    static Path restoreBasePath;

    // File containing ETFS
    static Path tapeFile;
    static Path tapeFileCompressed;

    // Size of each file generated fo the test (37kb)
    static final int fileSizeBytes = 37 * 1024;
    // Number of files to generate for the test
    static final int numFiles = 23;
    // Seed for the random number generator used to generate and verify file contetns
    static final int randomSeed = 47355811;
    // Every n file will only be restored in the partial restore test
    static final int partialRestoreMod = 5;

    // Plugins and co are not tested here, as that is already done in ETFS
    // this will only test if read and write access (including skipping) is
    // working as expected

    @BeforeClass
    public static void prepareTestFiles() throws IOException {
        Random r = new Random(randomSeed);
        byte[] fileContent = new byte[fileSizeBytes];

        for (int i = 0; i < numFiles; i++) {
            r.nextBytes(fileContent);
            inputFiles.add(Files.createTempFile("JTAPE-FILE-IMPL-TEST-IN-FILE", ".bin"));
            Files.write(inputFiles.get(i), fileContent);
        }

        restoreBasePath = Files.createTempDirectory("JTAPE-FILE-IMPL-TEST-OUT-FOLDER");
        tapeFile = Files.createTempFile("JTAPE-FILE-IMPL-TEST-TAPE-FILE", "etfs");
        tapeFileCompressed = Files.createTempFile("JTAPE-FILE-IMPL-TEST-TAPE-FILE", "etfs.gz");
    }

    @AfterClass
    public static void cleanup() throws IOException {
        inputFiles.forEach(f -> {
            try {
                Files.delete(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Files.delete(restoreBasePath);
    }

    @After
    public void cleanupOutFolder() throws IOException {
        // Clean up data in folder
        Files.list(restoreBasePath).forEach(path -> {
            try {
                Files.delete(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void createAndRestoreTape(Path tapeFile, FileSelector fs) throws IOException {
        // Step 0: Delete file if already exists
        Files.deleteIfExists(tapeFile);

        // Test 1: Create a tape
        FileBasedTapeDrive tapeDrive = FileBasedTapeDrive.createForFile(tapeFile, FileBasedTapeDrive.STREAM_MODE.WRITE);
        EtfsInstance etfs = EtfsWriteDirectionBuilder.newInstance()
                .addFiles(inputFiles)
                .withLabel("FILE_TAPE_TEST")
                .withTapeSpecificOperationDispatcher(new NoopTapeSpecificOperationDispatcher())
                .build();
        etfs.serialize(tapeDrive.getOutputStream(), new EtfsNoopWriteProgressListener());

        // Step 2: Read back files to FS
        tapeDrive = FileBasedTapeDrive.createForFile(tapeFile, FileBasedTapeDrive.STREAM_MODE.READ);
        etfs = EtfsReadDirectionBuilder.
                newInstance(new PathRemapper() {
                    Map<String, Integer> positionInFileList = new HashMap<>();

                    @Override
                    public void prepareFileMapping(List<FileEntry> allFiles) {
                        for (int i = 0; i < allFiles.size(); i ++) {
                            positionInFileList.put(allFiles.get(i).getFilename(), i);
                        }
                    }

                    @Override
                    public Path map(String normalizedPath) {
                        return restoreBasePath.resolve(positionInFileList.get(normalizedPath).toString());
                    }
                })
                .withFileSelector(fs)
                .withTapeSpecificOperationDispatcher(new NoopTapeSpecificOperationDispatcher())
                .build();
        try {
            etfs.deserialize(tapeDrive.getInputStream(), new EtfsNoopReadProgressListener());
        } catch (Throwable t) {
            t.printStackTrace();
        }

        validateFileContents(restoreBasePath, randomSeed);
    }

    /**
     * Takes a folder with numbered files. Each number corresponds to the call index to
     * Random#nextBytes(). This method will than generate the right amount of random data
     * and check it against the file content
     *
     * @param baseFolder
     * @param seed
     * @throws IOException
     */
    public void validateFileContents(Path baseFolder, int seed) throws IOException {
        List<Path> filesInFolder = Files.list(baseFolder).collect(Collectors.toList());
        filesInFolder.sort(new Comparator<Path>() {
            @Override
            public int compare(Path o1, Path o2) {
                return Integer.parseInt(o1.getFileName().toString()) - Integer.parseInt(o2.getFileName().toString());
            }
        });

        Random r = new Random(seed);
        byte[] fileContent;
        byte[] randomContent = new byte[fileSizeBytes];
        int lastFileNum = 0;
        int entriesSkipped = 0;

        for (Path p : filesInFolder) {
            fileContent = Files.readAllBytes(p);

            // Generate the random data for the files skipped when restoring
            // (e.g. only files 0, 3 and 6 exist. This means that data for
            // 1, 2, 4 and 5 needs to be generated, but discarded)
            entriesSkipped = Integer.parseInt(p.getFileName().toString()) - lastFileNum - 1;
            for (int j = 0; j < entriesSkipped; j++) {
                r.nextBytes(randomContent);
            }

            r.nextBytes(randomContent);
            //Arrays.fill(randomContent, p.getFileName().toString().getBytes(StandardCharsets.UTF_8)[0]);
            Assert.assertArrayEquals(
                    "Restored file content does not match expected content (File "+p+")",
                    randomContent,
                    fileContent
            );

            lastFileNum = Integer.parseInt(p.getFileName().toString());
        }
    }

    @Test
    public void createAndRestoreNormalTape() throws IOException {
        // Test 1: Without skipping
        createAndRestoreTape(tapeFile, new AcceptAllFileSelector());

        // Test 2: With skipping
        createAndRestoreTape(tapeFile, new FileSelector() {
            int i = 0;

            @Override
            public void prepareFileSelection(List<FileEntry> allFiles) {

            }

            @Override
            public boolean writeFile(FileEntry file) {
                boolean res = i % partialRestoreMod == 0;
                i++;
                return res;
            }
        });
    }

    @Test
    public void createAndRestoreCompressedTape() throws IOException {
        // Test 1: Without skipping
        createAndRestoreTape(tapeFileCompressed, new AcceptAllFileSelector());

        // Test 2: With skipping
        createAndRestoreTape(tapeFileCompressed, new FileSelector() {
            int i = 0;

            @Override
            public void prepareFileSelection(List<FileEntry> allFiles) {

            }

            @Override
            public boolean writeFile(FileEntry file) {
                boolean res = i % partialRestoreMod == 0;
                i++;
                return res;
            }
        });
    }
}
