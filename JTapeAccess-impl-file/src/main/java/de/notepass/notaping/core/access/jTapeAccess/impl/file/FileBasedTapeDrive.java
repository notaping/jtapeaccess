package de.notepass.notaping.core.access.jTapeAccess.impl.file;

import de.notepass.notaping.core.access.jTapeAccess.impl.stream.StreamBasedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.*;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class FileBasedTapeDrive extends StreamBasedTapeDrive {
    private Path tapeFile;

    boolean compression = false;

    public static FileBasedTapeDrive createForFile(String file, STREAM_MODE streamMode) throws IOException {
        return createForFile(Paths.get(file), streamMode);
    }

    public static FileBasedTapeDrive createForFile(Path file, STREAM_MODE streamMode) throws IOException {
        FileBasedTapeDrive ftd = new FileBasedTapeDrive();
        ftd.setStreamMode(streamMode);
        ftd.setTapeDriveFile(file);
        ftd.load();

        return ftd;
    }

    public void unload() throws IOException {
        super.unload();
        tapeFile = null;
    }

    @Override
    public void forwardFilemarks(int amount) throws TapeException {
        throw new IllegalStateException("File based volumes do not support filemark skipping");
    }

    @Override
    public void reverseFilemarks(int amount) throws TapeException {
        throw new IllegalStateException("File based volumes do not support filemark skipping");
    }

    @Override
    public DriveCapabilities[] getDriveCapabilities() throws TapeException {
        return new DriveCapabilities[]{DriveCapabilities.COMPRESSION};
    }

    @Override
    public void enableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {
        if (Arrays.stream(driveCapabilities).anyMatch(c -> c == DriveCapabilities.COMPRESSION)) {
            if (isMediaPresent()) {
                throw new IllegalStateException("Compression can only be enabled/disabled before calling load()");
            }
            compression = true;
        }
    }

    @Override
    public void disableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {
        if (Arrays.stream(driveCapabilities).anyMatch(c -> c == DriveCapabilities.COMPRESSION)) {
            if (isMediaPresent()) {
                throw new IllegalStateException("Compression can only be enabled/disabled before calling load()");
            }
            compression = false;
        }
    }

    @Override
    public TapeInfo getTapeInfo() throws TapeException {
        try {
            FileStore fs = tapeFile.getFileSystem().getFileStores().iterator().next();

            return new TapeInfo(
                    fs.getUnallocatedSpace(),
                    fs.getTotalSpace() - fs.getUnallocatedSpace(),
                    fs.getTotalSpace(),
                    getStreamMode() == STREAM_MODE.READ,
                    getBlockSize()
                );
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Error while accessing space information", e);
        }
    }

    public void setTapeDriveFile(Path tapeFile) throws TapeException {
        if (isMediaPresent()) {
            throw new IllegalStateException("Call unload() before calling setTapeDriveFile()");
        }

        if (getStreamMode() == STREAM_MODE.WRITE && Files.exists(tapeFile)) {
            throw new IllegalArgumentException("Output file " + tapeFile + " already exists");
        } else if (getStreamMode() == STREAM_MODE.READ && !Files.exists(tapeFile)) {
            throw new IllegalArgumentException("Input file " + tapeFile + " does not exist");
        }

        this.tapeFile = tapeFile;
    }

    @Override
    public void load() throws IOException {
        boolean compression = tapeFile.getFileName().toString().endsWith(".gz");
        if (compression) {
            enableDriveCapability(DriveCapabilities.COMPRESSION);
        }

        if (getStreamMode() == STREAM_MODE.READ) {
            try {
                setStream(Files.newInputStream(tapeFile));
            } catch (IOException e) {
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not create input stream for input file "+tapeFile);
            }
        } else if (getStreamMode() == STREAM_MODE.WRITE) {
            try {
                setStream(Files.newOutputStream(tapeFile));
            } catch (IOException e) {
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not create output stream for output file "+tapeFile);
            }
        } else {
            throw new IllegalStateException("Unknown STREAM_MODE: "+getStreamMode());
        }

        super.load();
    }

    @Override
    public void setTapeDriveFile(String tapeDriveFile) throws TapeException {
        setTapeDriveFile(Paths.get(tapeDriveFile));
    }

    @Override
    public String getTapeDriveFile() {
        return tapeFile.toString();
    }
}
