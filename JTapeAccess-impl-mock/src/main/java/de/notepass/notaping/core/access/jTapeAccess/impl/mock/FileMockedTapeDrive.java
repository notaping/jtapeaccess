package de.notepass.notaping.core.access.jTapeAccess.impl.mock;

import de.notepass.notaping.core.access.jTapeAccess.impl.NoopTapeMetaDataListener;
import de.notepass.notaping.core.access.jTapeAccess.model.*;
import de.notepass.notaping.core.shared.DelegatingInputStream;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * A tape drive that isn't a tape drive, but a file. Used for testing purposes or to play around with the library
 */
public class FileMockedTapeDrive implements ExtendedTapeDrive {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileMockedTapeDrive.class);

    public enum WORK_DIRECTION {
        READ, WRITE
    }

    private String userProvidedTapeFilePath;
    private Path tapeFile;
    private Path tapeInfoFile;
    private List<Long> tapeFilemarkPositions;
    private OutputStream tapeOutputStream;
    private InputStream tapeInputStream;
    private boolean filemarkSupportEnabled;
    private WORK_DIRECTION workDirection;
    private boolean isReady = false;
    private long position;
    private long capacity;
    private int blockSize = 64 * 1024;
    private TapeInputStream userFacingInputStream;
    private BlockSizeCorrectingTapeInputStream correctingInputStream;
    private TapeOutputStream userFacingOutputStream;
    private BlockSizeCorrectingTapeOutputStream correctingOutputStream;
    private TapeMetadataEncounteredListener tapeMetadataEncounteredListener = new NoopTapeMetaDataListener();

    public WORK_DIRECTION getWorkDirection() {
        return workDirection;
    }

    public void setWorkDirection(WORK_DIRECTION workDirection) {
        this.workDirection = workDirection;
    }

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }

    /**
     * Return the index of the filemark last passed (Or currently on) in the tapeEofMarkPositions list
     * @return
     */
    private int getCurrentFilemarkIndex() {
        // No EOF marks exist or position is before first mark
        if (tapeFilemarkPositions.size() <= 0 || position <= tapeFilemarkPositions.get(0)) {
            return 0;
        }

        // Position is after last mark
        if (position >= tapeFilemarkPositions.get(tapeFilemarkPositions.size() - 1)) {
            return tapeFilemarkPositions.size() - 1;
        }

        for (int i = 0; i < tapeFilemarkPositions.size() - 1; i++) {
            long currentFileMark = tapeFilemarkPositions.get(i);
            long nextFileMark = tapeFilemarkPositions.get(i + 1);
            if (position >= currentFileMark && position < nextFileMark) {
                return i;
            }
        }

        // Unkown position
        LOGGER.warn("Could not determinate nearest filemark position, returning 0");
        return 0;
    }

    public boolean isFilemarkSupportEnabled() {
        return filemarkSupportEnabled;
    }

    public void setFilemarkSupportEnabled(boolean filemarkSupportEnabled) {
        this.filemarkSupportEnabled = filemarkSupportEnabled;
    }

    /**
     * Sets all needed properties for loadTape()
     */
    public void prepare(String tapeFile, boolean filemarkSupportEnabled, WORK_DIRECTION workDirection, long capacity) {
        this.userProvidedTapeFilePath = tapeFile;
        this.filemarkSupportEnabled = filemarkSupportEnabled;
        this.workDirection = workDirection;
        this.capacity = capacity;
    }

    @Override
    public void loadTape() throws TapeException {
        // Check if work direction is set properly
        if (workDirection != WORK_DIRECTION.READ && workDirection != WORK_DIRECTION.WRITE) {
            throw new TapeException(TapeException.ErrorCode.NO_TAPE_IN_DRIVE, "Please first call setWorkDirection before calling loadTape()");
        }

        try {
            // Check if the tape drive already has a virtual tape assigned
            if (tapeInputStream != null || tapeOutputStream != null) {
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Please call ejectTape() before loading a new file into an active instance!");
            }

            if (userProvidedTapeFilePath == null || userProvidedTapeFilePath.isEmpty()) {
                throw new TapeException(TapeException.ErrorCode.NO_TAPE_IN_DRIVE, "Please first set a tape file by calling setTapeDriveFile() and then call loadTape()!");
            }

            if (workDirection == null) {
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Configure if this mock instance is used for reading or writing first with setWorkDirection()");
            }

            // If not, parse string to path and assign info file
            tapeFile = Path.of(userProvidedTapeFilePath);
            tapeInfoFile = Path.of(tapeFile.toString() + ".eof.properties");

            // Prepare mock either for read or write direction (Bidirectional as with normal tapes is not possible)
            if (workDirection == WORK_DIRECTION.WRITE) {
                // Steps for creating an empty tape to write to

                // Initialize filemarks as empty
                this.tapeFilemarkPositions = new ArrayList<>();

                // If tape file does not exist, create folder structure and file
                if (!Files.exists(tapeFile)) {
                    try {
                        Files.createDirectories(tapeFile.getParent());
                        Files.createFile(tapeFile);
                    } catch (IOException e) {
                        throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not create tape file/directory structure", e);
                    }
                }

                // Create and assign TapeOutputStream
                tapeOutputStream = Files.newOutputStream(tapeFile);

                // Create properties file if not exists. if it exists, it will be overwritten by ejectTape()
                if (!Files.exists(tapeInfoFile) && filemarkSupportEnabled) {
                    try {
                        Files.createFile(tapeInfoFile);
                    } catch (IOException e) {
                        throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not create EOF info file for tape file", e);
                    }
                }

            } else if (workDirection == WORK_DIRECTION.READ) {
                // Prepare for usage of an existing tape filr

                // Check if file actually exists
                if (!Files.exists(tapeFile)) {
                    throw new TapeException(TapeException.ErrorCode.NO_TAPE_IN_DRIVE, "Input file for tape drive does not exist: " + userProvidedTapeFilePath);
                }

                // Initialize empty properties object which is used to load properties
                Properties tapeProperties = new Properties();

                // Check if the tape info file exists and load if so
                if (Files.exists(tapeInfoFile)) {
                    // load tape metadata from file
                    try (BufferedReader br = Files.newBufferedReader(tapeInfoFile)) {
                        tapeProperties.load(br);
                    } catch (IOException e) {
                        throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not read tape info file", e);
                    }
                } else {
                    // Required metadata are missing
                    throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Trying to load tape file without info file");
                }

                // Create and assign TapeInputStream for this mock drive
                tapeInputStream = Files.newInputStream(tapeFile);

                // Parse info file metadata in fields
                filemarkSupportEnabled = Boolean.parseBoolean(tapeProperties.getProperty("filemark.enable"));
                blockSize = Integer.parseInt(tapeProperties.getProperty("tape.blockSize"));
                capacity = Long.parseLong(tapeProperties.getProperty("tape.capacity"));

                // Read filemark positions from info file
                tapeFilemarkPositions = tapeProperties
                        .keySet()
                        .stream()
                        .filter(o -> o.toString().startsWith("filemark.position."))
                        .map(o -> tapeProperties.getProperty(o.toString()))
                        .map(Long::parseLong)
                        .sorted(Long::compareTo)
                        .collect(Collectors.toList());
            } else {
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Unknown direction: " + getWorkDirection());
            }
        } catch (Exception e) {
            LOGGER.error("Error detected, calling ejectTape to reset to default condition");
            ejectTape();
            if (e instanceof TapeException) {
                throw (TapeException) e;
            } else {
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "An unknown error occurred", e);
            }
        }

        isReady = true;
        invalidateInputStream();
    }

    @Override
    public void ejectTape() throws TapeException {
        isReady = false;

        if (workDirection == WORK_DIRECTION.WRITE) {

            if (tapeInfoFile != null) {
                // Save metadata if possible
                Properties tapeProperties = new Properties();
                tapeProperties.setProperty("filemark.enable", Boolean.toString(filemarkSupportEnabled));
                tapeProperties.setProperty("tape.blockSize", Integer.toString(blockSize));
                tapeProperties.setProperty("tape.capacity", Long.toString(capacity));


                if (filemarkSupportEnabled && tapeFilemarkPositions != null) {
                    for (int i = 0; i < tapeFilemarkPositions.size(); i++) {
                        tapeProperties.setProperty("filemark.position."+i, Long.toString(tapeFilemarkPositions.get(i)));
                    }
                }

                // Write metadata file
                try (OutputStream os = Files.newOutputStream(tapeInfoFile)) {
                    tapeProperties.store(os, "");
                } catch (IOException e) {
                    throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not write tape info file. Tape file will not be readable!", e);
                }
            } else {
                LOGGER.warn("Info file is not set. Tape info will not be written and tape file will not be readable!");
            }
        }

        // Close backing streams
        if (tapeOutputStream != null) {
            try {
                tapeOutputStream.flush();
                tapeOutputStream.close();
            } catch (IOException e) {
                LOGGER.warn("Could not correctly close outputStream for tape file", e);
            }
        }

        if (tapeInputStream != null) {
            try {
                tapeInputStream.close();
            } catch (IOException e) {
                LOGGER.warn("Could not correctly close inputStream for tape file", e);
            }
        }

        // Reset variables
        userProvidedTapeFilePath = null;
        tapeOutputStream = null;
        tapeInputStream = null;
        tapeFilemarkPositions = null;
        tapeInfoFile = null;
        tapeFile = null;
        position = 0;
        workDirection = null;

        invalidateInputStream();
    }

    //@Override
    //public void partitionTape() throws TapeException {
    //    throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Unsupported operation: partitionTape");
    //}

    @Override
    public void setTapeMetadataListener(TapeMetadataEncounteredListener listener) {
        tapeMetadataEncounteredListener = listener;
    }

    @Override
    public OS getTargetOs() {
        return null;
    }

    @Override
    public boolean isReadable() throws TapeException {
        return isReady && tapeInputStream != null;
    }

    @Override
    public boolean isWritable() throws TapeException {
        return isReady && tapeOutputStream != null;
    }

    @Override
    public long getTapeBlockPosition() throws TapeException {
        return position / blockSize;
    }

    private void setPosition(long targetPos) throws TapeException {
        if (workDirection != WORK_DIRECTION.READ) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Setting tape position is only supported in READ mode");
        }

        if (position > targetPos) {
            // Position is after current position. Reset stream
            LOGGER.warn("Setting stream position to position before the current one. Recreating input stream.");
            try {
                tapeInputStream.close();
                tapeInputStream = Files.newInputStream(tapeFile);
                position += tapeInputStream.skip(targetPos);
            } catch (IOException e) {
                ejectTape();
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Error while recreating InputStream while trying to reverse. INVALID STATE, ejecting virtual tape");
            }
        } else {
            try {
                position += tapeInputStream.skip(targetPos - position);
            } catch (IOException e) {
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not skip forward "+(targetPos - position)+" bytes", e);
            }
        }

        //if (position != targetPos) {
        //    throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Current position is not expected position. Expected:"+targetPos+"; Actual: "+position);
       //}

        invalidateInputStream();
    }

    @Override
    public void setTapeBlockPosition(long pos) throws TapeException {
        setPosition(pos * blockSize);

        invalidateInputStream();
    }

    @Override
    public void forwardFilemarks(int amount) throws TapeException {
        if (workDirection != WORK_DIRECTION.READ) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Setting tape position is only supported in READ mode");
        }

        int targetFilemarkIndex = getCurrentFilemarkIndex() + amount;

        // Check if target is outside virtual tape
        if (targetFilemarkIndex > tapeFilemarkPositions.size() - 1) {
            // Skip to end of file
            try {
                position += tapeInputStream.skip(Long.MAX_VALUE);
            } catch (IOException e) {
                ejectTape();
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not skip to end of file. INVALID STATE. Ejecting virtual tape", e);
            }

            // Return end of tape error
            throw new TapeException(TapeException.ErrorCode.NO_SPACE_LEFT, "Skipped to end of virtual tape file, as target filemark does not exist in it");
        } else {
            // Filemark inside virtual tape, skip to it
            setPosition(tapeFilemarkPositions.get(targetFilemarkIndex));
        }

        invalidateInputStream();
    }

    @Override
    public void reverseFilemarks(int amount) throws TapeException {
        if (workDirection != WORK_DIRECTION.READ) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Setting tape position is only supported in READ mode");
        }

        int targetFilemarkIndex = getCurrentFilemarkIndex() + amount;

        // Check if target is outside virtual tape
        if (targetFilemarkIndex < 0) {
            // Skip to start of file
            setPosition(0);

            // Return end of tape error
            throw new TapeException(TapeException.ErrorCode.INVALID_TAPE_POSITION, "Skipped to start of virtual tape file, as target filemark does not exist in it");
        } else {
            // Filemark inside virtual tape, skip to it
            setPosition(tapeFilemarkPositions.get(targetFilemarkIndex));
        }

        invalidateInputStream();
    }

    @Override
    public DriveCapabilities[] getDriveCapabilities() throws TapeException {
        return new DriveCapabilities[0];
    }

    @Override
    public void enableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {

    }

    @Override
    public void disableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {

    }

    @Override
    public void setMediumBlockSize(int blockSizeBytes) throws TapeException {
        blockSize = blockSizeBytes;
    }

    @Override
    public int readBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        int bytesToRead = numBytes - offset;

        if (workDirection != WORK_DIRECTION.READ) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Reading is only supported in READ mode");
        }

        try {
            if (position + bytesToRead > capacity) {
                int readableBytes = (int) (capacity - position);
                readBytes(buffer, offset, readableBytes);
                return readableBytes;
            }

            int bytesRead = tapeInputStream.read(buffer, offset, numBytes);
            int oldFilemarkIndex = getCurrentFilemarkIndex();
            position += bytesRead;

            if (oldFilemarkIndex != getCurrentFilemarkIndex()) {
                // Stepped over a filemark, report
                tapeMetadataEncounteredListener.onFilemarkEncountered();
            }
            return bytesRead;
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not read from tape file", e);
        }
    }

    @Override
    public int writeBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        if (workDirection != WORK_DIRECTION.WRITE) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Writing is only supported in WRITE mode");
        }

        if (numBytes + position > capacity) {
            int writableBytes = (int) (capacity - position);
            return writeBytes(buffer, offset, writableBytes);
        }

        try {
            tapeOutputStream.write(buffer, offset, numBytes);
            position += numBytes;
            return numBytes;
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not write to tape file", e);
        }
    }

    @Override
    public int readBytes(byte[] buffer) throws TapeException {
        return readBytes(buffer, 0, buffer.length);
    }

    @Override
    public int writeBytes(byte[] buffer) throws TapeException {
        return writeBytes(buffer, 0, buffer.length);
    }

    @Override
    public void writeFilemarks(int amount) throws TapeException {
        for (int i = 0; i < amount; i++) {
            tapeFilemarkPositions.add(position);
        }

        tapeFilemarkPositions.sort(Long::compareTo);
    }

    @Override
    public WrittenBytesAwareOutputStream getOutputStream() throws TapeException {
        if (correctingOutputStream == null) {
            // Make sure raw stream is initialized
            getRawOutputStream();
            correctingOutputStream = new BlockSizeCorrectingTapeOutputStream<TapeOutputStream>(userFacingOutputStream);
        }

        return correctingOutputStream;
    }

    @Override
    public InputStream getInputStream() throws TapeException {
        if (correctingInputStream == null) {
            // Make sure raw stream is initialized
            getRawInputStream();
            correctingInputStream = new BlockSizeCorrectingTapeInputStream<TapeInputStream>(userFacingInputStream);
        }

        return correctingInputStream;
    }

    @Override
    public WrittenBytesAwareOutputStream getRawOutputStream() throws TapeException {
        if (workDirection != WORK_DIRECTION.WRITE) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Writing is only supported in WRITE mode");
        }

        if (userFacingOutputStream == null) {
            userFacingOutputStream = new TapeOutputStream(this);
        }

        return userFacingOutputStream;
    }

    @Override
    public InputStream getRawInputStream() throws TapeException {
        if (workDirection != WORK_DIRECTION.READ) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Reading is only supported in READ mode");
        }

        if (userFacingInputStream == null) {
            userFacingInputStream = new TapeInputStream(this);
        }

        return userFacingInputStream;
    }

    @Override
    public void flushCaches() throws TapeException {
        if (workDirection == WORK_DIRECTION.WRITE) {
            try {
                tapeOutputStream.flush();
            } catch (IOException e) {
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not flush stream", e);
            }
        }
    }

    @Override
    public boolean isMediaPresent() throws TapeException {
        return isReady;
    }

    @Override
    public TapeInfo getTapeInfo() throws TapeException {
        long fileSize = 0;
        try {
            fileSize = Files.size(tapeFile);
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not determinate current file size");
        }
        return new TapeInfo(this.capacity - fileSize, fileSize, capacity, false, blockSize);
    }

    @Override
    public void setTapeDriveFile(String tapeDriveFile) throws TapeException {
        if (isReady) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Please call ejectTape before switching tape files");
        }

        userProvidedTapeFilePath = tapeDriveFile;
    }

    @Override
    public String getTapeDriveFile() {
        return userProvidedTapeFilePath;
    }

    private void invalidateInputStream() {
        if (correctingInputStream != null) {
            correctingInputStream.invalidate();
        }
    }
}
