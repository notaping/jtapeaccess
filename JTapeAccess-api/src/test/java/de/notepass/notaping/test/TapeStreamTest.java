package de.notepass.notaping.test;

import de.notepass.notaping.core.access.jTapeAccess.model.BlockSizeCorrectingTapeInputStream;
import de.notepass.notaping.core.access.jTapeAccess.model.BlockSizeCorrectingTapeOutputStream;
import de.notepass.notaping.core.shared.SimpleWrittenBytesAwareOutputStreamWrapper;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Random;

public class TapeStreamTest {
    public static final int BLOCK_SIZE_BYTES = 64;

    @Test
    public void correctingTapeOutputStreamTest() throws IOException {

        StringBuilder shorterThanBuffer = new StringBuilder();
        shorterThanBuffer.append("1".repeat(BLOCK_SIZE_BYTES - 5));

        StringBuilder sameSizeAsBuffer = new StringBuilder();
        sameSizeAsBuffer.append("2".repeat(BLOCK_SIZE_BYTES));

        StringBuilder biggerThanBuffer = new StringBuilder();
        biggerThanBuffer.append("3".repeat(BLOCK_SIZE_BYTES * 2));

        byte[] expectedResult = new byte[BLOCK_SIZE_BYTES * 4];

        byte[] subArr = shorterThanBuffer.toString().getBytes(StandardCharsets.UTF_8);
        int index = writeSubArray(expectedResult, subArr, 0);

        subArr = new byte[5];
        Arrays.fill(subArr, (byte) 0);
        index = writeSubArray(expectedResult, subArr, index);

        subArr = sameSizeAsBuffer.toString().getBytes(StandardCharsets.UTF_8);
        index = writeSubArray(expectedResult, subArr, index);

        subArr = biggerThanBuffer.toString().getBytes(StandardCharsets.UTF_8);
        index = writeSubArray(expectedResult, subArr, index);


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BlockSizeCorrectingTapeOutputStream tos = new BlockSizeCorrectingTapeOutputStream(new SimpleWrittenBytesAwareOutputStreamWrapper(baos), BLOCK_SIZE_BYTES);

        tos.write(shorterThanBuffer.toString().getBytes(StandardCharsets.UTF_8));
        tos.flush();

        tos.write(sameSizeAsBuffer.toString().getBytes(StandardCharsets.UTF_8));
        tos.flush();

        tos.write(biggerThanBuffer.toString().getBytes(StandardCharsets.UTF_8));
        tos.flush();

        Assert.assertArrayEquals("BufferedTapeOutputStream generated invalid output", expectedResult, baos.toByteArray());
    }

    @Test
    public void bufferedTapeInputStreamTest() throws IOException {
        Assert.assertTrue("BUFFER_SIZE_BYTES is not dividable by 2!", BLOCK_SIZE_BYTES %2 == 0);

        long randomSeed = 123456789;

        Random r = new Random(randomSeed);

        byte[] generatedBytes = new byte[BLOCK_SIZE_BYTES * 4];
        byte[] readBytes = new byte[generatedBytes.length];
        r.nextBytes(generatedBytes);

        ByteArrayInputStream bais = new ByteArrayInputStream(generatedBytes);

        BlockSizeCorrectingTapeInputStream tapeInputStream = new BlockSizeCorrectingTapeInputStream(bais, BLOCK_SIZE_BYTES);
        int readSize = BLOCK_SIZE_BYTES * 2;
        int index = 0;

        // Try different read sizes, to make sure that there aren't any problems there
        while (readSize > 1) {
            Assert.assertEquals("A different amount of bytes was read fom the buffered input stream than requested", readSize, tapeInputStream.read(readBytes, index, readSize));

            //Extract subarray for direct testing
            byte[] subArrIs = new byte[readSize];
            byte[] subArrExpected = new byte[readSize];
            System.arraycopy(readBytes, index, subArrIs, 0, readSize);
            System.arraycopy(generatedBytes, index, subArrExpected, 0, readSize);
            Assert.assertArrayEquals("Subarray while reading "+readSize+" bytes from buffer doesn't match", subArrExpected, subArrIs);

            index += readSize;
            readSize = readSize/2;

            Assert.assertTrue("Too many entries have been written into the target array (readSize="+(readSize*2)+")", readBytes[index] == 0);
        }

        //These checks aren't needed anymore, as all checks are done in the loop
        //Workaround, because using the correct array size would throw an index out of bounds in the tape stream
        //readBytes[readBytes.length - 1] = generatedBytes[readBytes.length - 1];

        //Assert.assertArrayEquals("TapeInputStream returned not the data it was fed with", generatedBytes, readBytes);
    }

    public static int writeSubArray(byte[] arr, byte[] subArr, int pos) {
        System.arraycopy(subArr, 0, arr, pos, subArr.length);
        return pos + subArr.length;
    }
}
