package de.notepass.notaping.core.access.jTapeAccess.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Extremely minimalistic InputStream which uses the tape drive interface for communications
 */
public class TapeInputStream extends InputStream {
    protected final SimpleTapeDrive tapeDevice;
    private static final Logger LOGGER = LoggerFactory.getLogger(TapeInputStream.class);

    public TapeInputStream(SimpleTapeDrive td) {
        tapeDevice = td;
    }

    @Override
    public int read() throws IOException {
        byte[] bytes = new byte[1];
        tapeDevice.readBytes(bytes);
        return bytes[0];
    }

    /**
     * Skips the given amount of bytes by setting the position of the tape.
     * This will put the drive in high speed transfer mode and should be faster than reading bytes directly<br/>
     * <b>Please note</b>: Skipping is only possible in multiples of the tape block size. Use a BufferedTapeInputStream
     * for jumping misc sizes.
     * @param n Number of bytes to skip
     * @return
     * @throws IOException
     */
    @Override
    public long skip(long n) throws IOException {
        LOGGER.debug("TapeInputStream: ENTER: skip("+n+")");

        if (n == 0) {
            return 0;
        }

        if (n < 0) {
            throw new IllegalArgumentException("Skip size may not be negative, but is "+n);
        }

        long blockSize = tapeDevice.getTapeInfo().getBlockSizeBytes();

        final long physicalBlockPosition = tapeDevice.getTapeBlockPosition();
        final long physicalBytePosition = physicalBlockPosition * blockSize;

        // Step 1: Convert number of bytes to blocks
        if (n % blockSize != 0) {
            throw new IOException("Skipping is only possible in byte multiples of the block size ("+blockSize+"). Request was a jump of "+n+"bytes");
        }
        final long numJumpBlocks = n / blockSize;
        LOGGER.debug("Step 1: Requested a jump size of "+numJumpBlocks+" blocks");

        // Step 2: Calculate absolute block target
        final long targetBlock = physicalBlockPosition + numJumpBlocks;
        LOGGER.debug("Step 2: Requested a to absolute target block "+targetBlock+" (Current position is "+physicalBlockPosition+")");

        // Step 3: Request jump from tape device
        tapeDevice.setTapeBlockPosition(targetBlock);
        LOGGER.debug("Step 3: Requested tape device to jump to block "+targetBlock);

        // Step 4: Get new position from tape device and calculate number of jumped bytes
        final long newPhysicalBlockPosition = tapeDevice.getTapeBlockPosition();
        final long newPhysicalBytePosition = newPhysicalBlockPosition * blockSize;
        final long numBytesSkipped = newPhysicalBytePosition - physicalBytePosition;
        final long numBlocksSkipped = newPhysicalBlockPosition - physicalBlockPosition;
        LOGGER.debug("Step 4: Tape device is now on block "+newPhysicalBlockPosition+" (thus skipping "+numBlocksSkipped+" blocks and "+numBytesSkipped+"bytes)");

        // Step 5: Return result
        return numBytesSkipped;
    }

    @Override
    public int read(byte[] b) throws IOException {
        return tapeDevice.readBytes(b);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return tapeDevice.readBytes(b, off, len);
    }
}
