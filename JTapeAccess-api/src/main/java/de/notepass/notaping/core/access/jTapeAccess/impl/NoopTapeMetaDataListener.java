package de.notepass.notaping.core.access.jTapeAccess.impl;

import de.notepass.notaping.core.access.jTapeAccess.model.TapeMetadataEncounteredListener;

public class NoopTapeMetaDataListener implements TapeMetadataEncounteredListener {
    @Override
    public void onFilemarkEncountered() {

    }
}
