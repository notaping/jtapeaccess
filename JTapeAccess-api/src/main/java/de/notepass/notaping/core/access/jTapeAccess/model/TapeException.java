package de.notepass.notaping.core.access.jTapeAccess.model;

import java.io.IOException;

public class TapeException extends IOException {
    private final ErrorCode errorCode;

    public static enum ErrorCode {
        /**
         * The given device file could not be found or accessed
         */
        DEVICE_NOT_FOUND,

        /**
         * No read operations are currently possible on the device but where attempted
         */
        DEVICE_NOT_READABLE,

        /**
         * No write operations are currently possible on the device but where attempted
         */
        DEVICE_NOT_WRITABLE,

        /**
         * There was an error, but no direct code exists for it. Might be an device, media, access, API, OS or whatever error
         */
        DEVICE_MISC_ERROR,

        /**
         * Tried to access tape information or read/write operations while there is no tape in the device
         */
        NO_TAPE_IN_DRIVE,

        /**
         * Tried to do an operation at an invalid tape position
         */
        INVALID_TAPE_POSITION,

        /**
         * Tried to do a write operation at the end of the tape
         */
        NO_SPACE_LEFT,

        /**
         * Tried to do a read operation at the end of the tape
         */
        // TODO: [Low] Remove, EOD/EOT should be detected by checking skip/read/write byte count
        END_OF_TAPE_REACHED,

        /**
         * The end of data on the tape was reached
         */
        END_OF_DATA_REACHED,

        /**
         * There are no native mappings to access tape drives for this OS<br/>
         * Maybe contribute to the project by implementing it? :)
         */
        NO_NATIVE_IMPL_AVAILABLE,

        /**
         * The setTapeDriveFile method was called on a SimpleTapeDrive implementation which already has a tape drive
         * assigned to it and cannot be reconfigured to the new tape drive
         */
        CLIENT_REINITIALIZATION_NOT_SUPPORTED,

        /**
         * an EOF mark was encountered while reading the tape
         */
        EOF_MARK_ENCOUNTERED;
    }

    public TapeException(ErrorCode errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public TapeException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
