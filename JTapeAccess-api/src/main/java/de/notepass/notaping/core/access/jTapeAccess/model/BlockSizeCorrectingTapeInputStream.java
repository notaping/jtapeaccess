package de.notepass.notaping.core.access.jTapeAccess.model;

import de.notepass.notaping.core.shared.DelegatingInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class BlockSizeCorrectingTapeInputStream<T extends InputStream> extends InputStream implements DelegatingInputStream<T> {

    private final byte[] blockBuffer;
    private final int blockSizeBytes;
    private int blockBufferPosition = 0;
    private final T parent;

    private static final Logger LOGGER = LoggerFactory.getLogger(BlockSizeCorrectingTapeInputStream.class);

    public BlockSizeCorrectingTapeInputStream(TapeInputStream parent) throws TapeException {
        this.blockBuffer = new byte[(int) parent.tapeDevice.getTapeInfo().getBlockSizeBytes()];
        this.blockSizeBytes = (int) parent.tapeDevice.getTapeInfo().getBlockSizeBytes();
        this.parent = (T) parent;
    }

    public BlockSizeCorrectingTapeInputStream(T parent, int blockSizeBytes) {
        this.blockBuffer = new byte[blockSizeBytes];
        this.blockSizeBytes = blockSizeBytes;
        this.parent = parent;
    }

    @Override
    public long skip(final long n) throws IOException {
        LOGGER.debug("BlockSizeCorrectingTapeInputStream: ENTER: skip("+n+")");

        LOGGER.debug("BlockSizeCorrectingTapeInputStream: Requested to skip "+n+" bytes (~ "+(n/blockSizeBytes)+" blocks)");

        if (n == 0) {
            return 0;
        }

        if (n < 0) {
            throw new IllegalArgumentException("Skip size may not be negative, but is "+n);
        }

        final int unreadBufferBytes = blockBuffer.length - blockBufferPosition;
        final int readBufferBytes = blockBufferPosition;

        if (unreadBufferBytes > n) {
            LOGGER.debug("Request can be served by just skipping in buffer ("+unreadBufferBytes+"bytes in buffer for "+n+"bytes request)");
            // Skipping can be done entirely in buffer
            return skipInBuffer((int) n);
        }

        long physicalSkipSize = n;
        long skipped = 0;

        LOGGER.debug("Physical skipping needed");
        // Physical skipping necessary
        // Step 1: Calculate the actual amount of bytes which need to be skipped on tape
        // Step 1a: Remove the blocks already read into buffer, as caller doesn't know they exist
        physicalSkipSize -= blockBuffer.length;
        LOGGER.debug("[PhysCalc] Step 1: Removed already read buffer block(s) of "+blockBuffer.length+"bytes. Remaining for physical skip: "+physicalSkipSize+"bytes");

        // Step 1b: Remove the "overhang" which would point into a block
        final int overhangBytes = (int) (physicalSkipSize%blockSizeBytes);
        physicalSkipSize -= overhangBytes;
        LOGGER.debug("[PhysCalc] Step 1: Removed overhang of "+overhangBytes+"bytes. Remaining for physical skip: "+physicalSkipSize+"bytes");

        // Step 1c: Add the bytes remaining in the buffer to the "skipped" variable, as they will be flushed by reading the next buffer after skipping
        skipped += unreadBufferBytes;
        LOGGER.debug("[SkipCalc] Step 1: Adding discarded buffer bytes to skipped. New value: "+skipped+"bytes");

        // Step 2: Call parent to skip the bytes on physical device (fast transport) and add the skipped bytes to the counter
        LOGGER.debug("[ExtCall] Step 2: Calling parent for skip of "+physicalSkipSize+"bytes (= "+(physicalSkipSize/blockSizeBytes)+" blocks)");
        skipped += parent.skip(physicalSkipSize);
        LOGGER.debug("[SkipCalc] Step 2: Adding skipped bytes by parent. New value: "+skipped+"bytes");

        // Step 3: Read in the overhang bytes removed in step 1b, as they can only be read in the buffer
        // Step 3a: Invalidate buffer
        invalidate();

        // Step 3b: Read overhang bytes (this will fill up the buffer) and add the amount skipped to the counter
        skipped += read(new byte[overhangBytes]);
        LOGGER.debug("[SkipCalc] Step 3: Adding discarded overhang bytes. New value: "+skipped+"bytes");

        // Step 3c: Readd the previous amount of buffer-read bytes
        skipped += read(new byte[readBufferBytes]);
        LOGGER.debug("[SkipCalc] Step 3: Readding the previous amount of buffer-read bytes. New value: "+skipped+"bytes");

        // Step 4: Return counter
        LOGGER.debug("[SkipCalc] Step 4: Returning value. FINAL value: "+skipped+"bytes");
        return skipped;
    }

    private long skipInBuffer(int n) throws IOException {
        final int unreadBufferBytes = blockBuffer.length - blockBufferPosition;

        if (n > unreadBufferBytes) {
            return skip(n);
        } else {
            return read(new byte[n]);
        }
    }

    @Override
    public int read() throws IOException {
        byte[] in = new byte[1];
        read(in);
        return in[0];
    }

    @Override
    public int read(byte[] b) throws IOException {
        return read(b, 0, b.length);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        int result = 0;
        int bytesRead = 0;

        if (blockBufferPosition > 0) {
            // There is still content in the buffer
            int bytesInBuffer = blockBuffer.length - blockBufferPosition;

            if (bytesInBuffer <= len) {
                result += bytesInBuffer;
                emptyBufferContent(b, off);

                off += bytesInBuffer;
                len -= bytesInBuffer;
            } else {
                result += len;
                readBufferPart(b, off, len);
                len = 0;
            }

            if (len == 0) {
                return result;
            }
        }

        int numTrailingBytesReadFromBuffer = len % blockBuffer.length;

        if (len - numTrailingBytesReadFromBuffer > 0) {
            bytesRead = parent.read(b, off, len - numTrailingBytesReadFromBuffer);
        }

        result += bytesRead;
        off += bytesRead;
        len -= bytesRead;

        assert blockBufferPosition == blockBuffer.length || blockBufferPosition == 0;

        if (numTrailingBytesReadFromBuffer > 0) {
            // Fill buffer and read from it in case of padding issues
            int numBytesReadIntoBuffer = fillBufferContent();
            if (numBytesReadIntoBuffer != blockBuffer.length) {
                // End of tape, write can be done and return
                if (numBytesReadIntoBuffer >= numTrailingBytesReadFromBuffer) {
                    // All bytes to read can be read from buffer
                    readBufferPart(b, off, numTrailingBytesReadFromBuffer);
                    result += numTrailingBytesReadFromBuffer;
                } else {
                    // Some bytes to read are out of bounds of the tape
                    readBufferPart(b, off, numBytesReadIntoBuffer);
                    result += numBytesReadIntoBuffer;
                }
            } else {
                // Could read full buffer, continue as normal
                readBufferPart(b, off, numTrailingBytesReadFromBuffer);
                result += numTrailingBytesReadFromBuffer;
            }
        }

        return result;
    }

    private void readBufferPart(byte[] target, int offset, int amount) {
        System.arraycopy(blockBuffer, blockBufferPosition, target, offset, amount);
        blockBufferPosition += amount;
    }

    private void emptyBufferContent(byte[] target, int offset) {
        System.arraycopy(blockBuffer, blockBufferPosition, target, offset, blockBuffer.length - blockBufferPosition);
        blockBufferPosition = blockBuffer.length;
    }

    private int fillBufferContent() throws IOException {
        blockBufferPosition = 0;
        int numBytesRead = parent.read(blockBuffer);
        if (numBytesRead != blockBuffer.length) {
            // Move read bytes to end and move position accordingly to emulate normal reading behaviour
            byte[] temp = new byte[numBytesRead];
            System.arraycopy(blockBuffer, 0, temp, 0, numBytesRead);
            System.arraycopy(temp, 0, blockBuffer, numBytesRead, numBytesRead);
            blockBufferPosition = blockBuffer.length - numBytesRead;
        }

        return numBytesRead;
    }

    /**
     * Invalidates the content of the block cache
     */
    public void invalidate() {
        emptyBufferContent(blockBuffer, 0);
        Arrays.fill(blockBuffer, (byte) 0);
    }

    @Override
    public T getDelegate() {
        return parent;
    }

    /**
     * Closes this and the delegate stream
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        super.close();
        getDelegate().close();
    }
}
