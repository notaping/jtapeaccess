package de.notepass.notaping.core.access.jTapeAccess.model;

/**
 * This class provides a listener interface to react to tape alternative datastream elements like EOF markers<br/>
 * This allows for not using exceptions to mark these things, like many native APIs use.
 */
public interface TapeMetadataEncounteredListener {
    public void onFilemarkEncountered();
}
