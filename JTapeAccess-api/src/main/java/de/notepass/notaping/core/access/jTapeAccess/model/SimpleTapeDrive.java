package de.notepass.notaping.core.access.jTapeAccess.model;

import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.InputStream;

/**
 * Basic tape drive functionallity. With this, it can be assesed if a tape dive can be worked with and basic data
 * can be read/written (This is limited to bytes and EOF marks)
 */
public interface SimpleTapeDrive {
    /**
     * Sets a user-defined tape metadata listener. If this is not set, a NOOP implementation should be used
     */
    void setTapeMetadataListener(TapeMetadataEncounteredListener listener);

    /**
     * Returns which OS this implementation targets
     * @return
     */
    OS getTargetOs();

    /**
     * Returns true if the media can be read from
     * @return
     * @throws TapeException
     */
    boolean isReadable() throws TapeException;

    /**
     * Returns true if the media can be written to
     * @return
     * @throws TapeException
     */
    boolean isWritable() throws TapeException;

    /**
     * Returns the current block the media is positioned at
     * @return
     * @throws TapeException
     */
    long getTapeBlockPosition() throws TapeException;

    /**
     * Sets the block address the drive should move the media to
     * @param pos
     * @throws TapeException
     */
    void setTapeBlockPosition(long pos) throws TapeException;

    /**
     * Fast forwards the media the given amounts of filemarks
     * The tape will be positioned after the filemark, so that the next read will return data behind it
     * @param amount The number of filemarks to skip
     * @throws TapeException
     */
    void forwardFilemarks(int amount) throws TapeException;

    /**
     * Reverses the media to given amounts of filemarks
     * The tape will be positioned after the filemark
     * @param amount The number of filemarks to reverse
     * @throws TapeException
     */
    void reverseFilemarks(int amount) throws TapeException;

    /**
     * Returns the functionallities the drive implements. These can be turned on or off via {@link #enableDriveCapability(DriveCapabilities)} and {@link #disableDriveCapability(DriveCapabilities)}
     * @return
     * @throws TapeException
     */
    DriveCapabilities[] getDriveCapabilities() throws TapeException;

    /**
     * Enabled a drive functionality
     * @param driveCapabilities
     * @throws TapeException
     */
    void enableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException;

    /**
     * Disables a drive functionality
     * @param driveCapabilities
     * @throws TapeException
     */
    void disableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException;

    /**
     * Sets the block size used for writing to and reading from the medium
     * A default value of 64KiB will be set on initialization.<br/>
     * After this property is set all read and write operations must be done with a multiple of the given block size. So make sure to use correcting streams!
     * {@link BlockSizeCorrectingTapeInputStream} and {@link BlockSizeCorrectingTapeOutputStream})<br/>
     * <b>Please note:</b> It is currently discouraged to use dynamic block sizing, as windows is just useless when enabling it!
     * @param blockSizeBytes
     */
    void setMediumBlockSize(int blockSizeBytes) throws TapeException;

    /**
     * Tires to read the given amount if bytes into the given buffer. Returns the actually read amount of bytes.
     * @param buffer Buffer to read data into
     * @param offset Offset inside the buffer into which to read the first byte
     * @param numBytes Number of bytes to read into the buffer
     * @return Actual number of bytes read
     * @throws TapeException
     */
    int readBytes(byte[] buffer, int offset, int numBytes) throws TapeException;

    /**
     * Tries to write the bytes given in the buffer-array onto the media. Returns the actually written amount of bytes
     * @param buffer Buffer to write onto the media
     * @param offset index of first byte to read from buffer and write to media
     * @param numBytes Number of bytes to write to the media
     * @return Actual number of bytes written
     * @throws TapeException
     */
    int writeBytes(byte[] buffer, int offset, int numBytes) throws TapeException;

    /**
     * Tires to read the given amount if bytes into the given buffer. Returns the actually read amount of bytes.
     * @param buffer Buffer to read data into
     * @return Actual number of bytes read
     * @throws TapeException
     */
    int readBytes(byte[] buffer) throws TapeException;


    /**
     * Tries to write the bytes given in the buffer-array onto the media. Returns the actually written amount of bytes
     * @param buffer Buffer to write onto the media
     * @return Actual number of bytes written
     * @throws TapeException
     */
    int writeBytes(byte[] buffer) throws TapeException;

    /**
     * Writes filemarks at the given position
     * @param amount Amount of filemarks to write
     * @throws TapeException
     */
    void writeFilemarks(int amount) throws TapeException;

    /**
     * Returns an OutputStream representation of the tape. This stream should provide secured access to
     * the tape which makes sure that things like writing in the correct block sizes are taken care of (e.g. by using
     * the buffered tape streams).<br>
     * This method also needs to take care of inheriting the cache status if the output stream is changed when calling it
     * @return
     * @throws TapeException
     */
    WrittenBytesAwareOutputStream getOutputStream() throws TapeException;

    /**
     * Returns an InputStream representation of the tape. This stream should provide secured access to
     *      * the tape which makes sure that things like writing in the correct block sizes are taken care of (e.g. by using
     *      * the buffered tape streams)
     * @return
     * @throws TapeException
     */
    InputStream getInputStream() throws TapeException;

    /**
     * Returns an OutputStream representation of the tape. This stream should provide raw access to
     * the tape without any buffers or safeguards to allow for low-level access (e.g. by using TapeOutputStream)
     * @return
     * @throws TapeException
     */
    WrittenBytesAwareOutputStream getRawOutputStream() throws TapeException;

    /**
     * Returns an InputStream representation of the tape. This stream should provide raw access to
     *      * the tape without any buffers or safeguards to allow for low-level access (e.g. by using TapeInputStream)
     * @return
     * @throws TapeException
     */
    InputStream getRawInputStream() throws TapeException;

    /**
     * Flush caches to media
     * @throws TapeException
     */
    void flushCaches() throws TapeException;

    /**
     * Returns true if a media is present int the drive
     * @return
     */
    boolean isMediaPresent() throws TapeException;

    /**
     * Returns information about the currently inserted tape
     * @return
     */
    TapeInfo getTapeInfo() throws TapeException;

    /**
     * Sets the path to the tape drive file. This should also initialize the connection to the tape drive. The implementation
     * does not need to implement switching of the tapeDriveFile and can throw an exception if an already assigned client
     * should be reconfigured to use another drive path
     * @param tapeDriveFile
     */
    void setTapeDriveFile(String tapeDriveFile) throws TapeException;

    /**
     * Returns the path to the tape drive file
     */
    String getTapeDriveFile();
}
