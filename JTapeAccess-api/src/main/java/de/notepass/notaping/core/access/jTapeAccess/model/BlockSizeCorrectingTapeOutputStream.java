package de.notepass.notaping.core.access.jTapeAccess.model;

import de.notepass.notaping.core.shared.DelegatingOutputStream;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

public class BlockSizeCorrectingTapeOutputStream<T extends WrittenBytesAwareOutputStream> extends WrittenBytesAwareOutputStream implements DelegatingOutputStream<T> {
    // Block size aligned buffer
    private final byte[] blockBuffer;

    private final int blockSize;

    // Position of the next byte to write
    private int blockBufferPosition = 0;

    // Parent stream to write into
    private final T parent;

    public BlockSizeCorrectingTapeOutputStream(TapeOutputStream parent) throws TapeException {
       this(parent, 1024);
    }
    public BlockSizeCorrectingTapeOutputStream(TapeOutputStream parent, int bufferMultiplier) throws TapeException {
        this((T) parent, (int) parent.tapeDevice.getTapeInfo().getBlockSizeBytes(), bufferMultiplier);
    }

    public BlockSizeCorrectingTapeOutputStream(T parent, int blockSizeBytes) {
        this(parent, blockSizeBytes, 1024);
    }

    public BlockSizeCorrectingTapeOutputStream(T parent, int blockSizeBytes, int bufferMultiplier) {
        blockSize = blockSizeBytes;
        this.blockBuffer = new byte[blockSizeBytes * bufferMultiplier];
        this.parent = parent;
    }

    @Override
    public int writeA(int i) throws IOException {
        return writeA(new byte[]{(byte) i});
    }

    @Override
    public int flushA() throws IOException {
        // Fill buffer and flush if applicable
        if (!isEmpty()) {
            // Amount of bytes to fill until the next block is full
            int toFill = toFill(true) - blockBufferPosition;
            if (toFill == blockSize) {
                toFill = 0;
            }
            writeIntoBuffer(new byte[toFill], 0, toFill);
            return flushBufferIfFull(true);
        }

        return 0;
    }

    @Override
    public int getExpectedNextFlushSize() {
        if (isEmpty()) {
            return 0;
        } else {
            return toFill(true);
        }
    }

    public int getBufferSize() {
        return blockBuffer.length;
    }

    public int getBlockSize() {
        return blockSize;
    }

    @Override
    public int writeA(byte[] val, int off, int len) throws IOException {
        return writeIntoBuffer(val, off, len);
    }

    @Override
    public int writeA(byte[] val) throws IOException {
        return writeIntoBuffer(val, 0, val.length);
    }

    private int writeIntoBuffer(byte[] data, int offset, int amount) throws IOException {
        int bytesWritten = 0;

        if (blockBufferPosition + amount > blockBuffer.length) {
            // not all data fits into buffer, only write part
            int toFill = toFill(false);
            System.arraycopy(data, offset, blockBuffer, blockBufferPosition, toFill);
            blockBufferPosition += toFill;
            offset += toFill;
            amount -= toFill;
            bytesWritten += toFill;
            int bufferWritten = flushBufferIfFull(false);

            if (bufferWritten != blockBuffer.length) {
                // if the buffer cannot be serialized (completely), return to sender with amount of bytes
                // written to the buffer, as that is the only thing committed in the data structure
                return toFill;
            }

            assert isEmpty();

            // then write-thought remaining data that can be written directly
            int writeDirectlyAmount = amount - (amount%blockBuffer.length);
            parent.write(data, offset, writeDirectlyAmount);

            offset += writeDirectlyAmount;
            amount -= writeDirectlyAmount;
            bytesWritten += writeDirectlyAmount;

            if (amount > 0) {
                // If there is still data to be written which didn't fit into a block, write it into buffer
                System.arraycopy(data, offset, blockBuffer, 0, amount);
                blockBufferPosition = amount; // +1 ?
                bytesWritten += amount;
            }
        } else {
            // Given data fits into buffer, just copy it into there
            System.arraycopy(data, offset, blockBuffer, blockBufferPosition, amount);
            blockBufferPosition += amount;
            bytesWritten += amount;
        }

        return bytesWritten;
    }

    /**
     * Writes buffer content to tape if the buffer is full
     * @param allowSmaller If set to true, the buffer doesn't need to bve completly full to be written to tape. It just
     *                     needs to be a multiple of the block size (or the block size itself)
     * @return Bytes written to the parent stream
     * @throws IOException
     */
    private int flushBufferIfFull(boolean allowSmaller) throws IOException {
        if (blockBufferPosition == blockBuffer.length) {
            int written = parent.writeA(blockBuffer);

            assert written <= blockBuffer.length;

            if (written != blockBuffer.length) {
                // Move unwritten data to front in array, so that the "back" part can be overwritten again
                byte[] unwrittenData = new byte[blockBuffer.length - written];
                System.arraycopy(blockBuffer, written, unwrittenData, 0, unwrittenData.length);
                System.arraycopy(unwrittenData, 0, blockBuffer, 0, unwrittenData.length);

                // Set write position to the first free index
                blockBufferPosition = blockBuffer.length - written;
            } else {
                blockBufferPosition = 0;
            }

            return written;
        } else if (allowSmaller && blockBufferPosition % blockSize == 0) {
            int written = parent.writeA(blockBuffer, 0, blockBufferPosition);

            assert written <= blockBufferPosition;

            if (written != blockBufferPosition) {
                // Move unwritten data to front in array, so that the "back" part can be overwritten again
                byte[] unwrittenData = new byte[blockBufferPosition - written];
                System.arraycopy(blockBuffer, written, unwrittenData, 0, unwrittenData.length);
                System.arraycopy(unwrittenData, 0, blockBuffer, 0, unwrittenData.length);

                // Set write position to the first free index
                blockBufferPosition = blockBufferPosition - written;
            } else {
                blockBufferPosition = 0;
            }

            return written;
        }

        return -1;
    }

    private int toFill(boolean allowSmaller) {
        if (allowSmaller) {
            if (blockBufferPosition == 0) {
                return 0;
            } else if (blockBufferPosition == blockBuffer.length) {
                return blockBufferPosition;
            } else {
                return blockBufferPosition + (blockSize - (blockBufferPosition % blockSize));
            }
        } else {
            return blockBuffer.length - blockBufferPosition;
        }
    }

    private boolean isEmpty() {
        return blockBufferPosition == 0;
    }

    /**
     * Integrates the buffer of another output stream into the buffer of this output stream to avoid loosing data
     * @param other Source stream
     * @param front If set to true the buffer will be "appended" to the ront instead of the back
     */
    public void integrateBuffer(BlockSizeCorrectingTapeOutputStream other, boolean front) throws IOException {
        byte[] otherBuffer = new byte[other.blockBufferPosition];
        System.arraycopy(other.blockBuffer, 0, otherBuffer, 0, otherBuffer.length);

        if (front) {
            byte[] currentBuffer = new byte[blockBufferPosition];
            System.arraycopy(blockBuffer, 0, currentBuffer, 0, currentBuffer.length);
            Arrays.fill(blockBuffer, (byte) 'Z');
            blockBufferPosition = 0;
            write(otherBuffer);
            write(currentBuffer);
        } else {
            write(otherBuffer);
        }
    }

    @Override
    public T getDelegate() {
        return parent;
    }

    /**
     * Flushes and closes this stream as well as the delegate
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        super.close();
        flushA();
        getDelegate().close();
    }
}
