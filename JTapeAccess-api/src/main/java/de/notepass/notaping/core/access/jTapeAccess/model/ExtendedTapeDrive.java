package de.notepass.notaping.core.access.jTapeAccess.model;

public interface ExtendedTapeDrive extends SimpleTapeDrive {
    /**
     * Loads the physical tape media into the tape drive (If present in a way this can be accomplished)
     * @throws TapeException
     */
    void loadTape() throws TapeException;

    /**
     * Ejects the tape from the tape drive
     * @throws TapeException
     */
    void ejectTape() throws TapeException;

    // Currently not active, will be added in the future
    ///**
    // * Creates a single partition on the tape, spanning the whole size of the tape     *
    // */
    //void partitionTape() throws TapeException;

    /**
     * Returns the current block the media is positioned at
     * @return
     * @throws TapeException
     */
    long getTapeBlockPosition() throws TapeException;

    /**
     * Sets the block address the drive should move the media to
     * @param pos
     * @throws TapeException
     */
    void setTapeBlockPosition(long pos) throws TapeException;
}
