package de.notepass.notaping.core.access.jTapeAccess.impl;

import de.notepass.notaping.core.access.jTapeAccess.model.TapeMetadataEncounteredListener;

import java.util.LinkedList;
import java.util.List;

/**
 * Sends the callbacks to multiple implementations
 */
public class MultiplexingTapeMetadataListener implements TapeMetadataEncounteredListener {
    private List<TapeMetadataEncounteredListener> listeners = new LinkedList<>();

    public void addListener(TapeMetadataEncounteredListener listener) {
        listeners.add(listener);
    }

    public boolean removeListener(TapeMetadataEncounteredListener listener) {
        return listeners.remove(listener);
    }

    @Override
    public void onFilemarkEncountered() {
        for (TapeMetadataEncounteredListener listener : listeners) {
            listener.onFilemarkEncountered();
        }
    }
}
