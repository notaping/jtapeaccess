package de.notepass.notaping.core.access.jTapeAccess;

import de.notepass.notaping.core.access.jTapeAccess.model.ExtendedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.SimpleTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.OS;
import de.notepass.notaping.core.access.jTapeAccess.model.TapeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;

public class SimpleTapeDriveUtils {
    private static Map<String, SimpleTapeDrive> simpleInstances = new HashMap<>();
    private static Map<String, ExtendedTapeDrive> extendedInstances = new HashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleTapeDriveUtils.class);

    /**
     * Returns a plattform-specif implementation of the SimpleTapeDrive class (or the ExtendedTapeDrive call, if available for the platform)
     * @param tapePath Path to the tape file to access the tape (e.g. \\.\TAPE0 on windows)
     * @param forceSimple By default, this method will return an ExtendedTapoeDrive-Implementation if it exists for the given OS. Set this
     *                    parameter to true to force the method to search for a SimpleTapeDriveBinding. Please note that some OS implementations only
     *                    prove ExtendedTapeDrive versions, thus this method might return null if this parameter is set to true, while having it set to
     *                    false will return a value. Because of this, <b>you should always use false, if not otherwise dictated by a specific need</b>
     * @return Plattform-specif implementation of the SimpleTapeDrive class
     * @throws TapeException If the drive cannot be correctly accessed the underlying system APIs, or if no implementation for the currently running system exists
     */
    public static synchronized SimpleTapeDrive getSimpleTapeDriveInstance(String tapePath, boolean forceSimple) throws TapeException {
        SimpleTapeDrive instance = null;

        // Query extended instances first, as they should be returned if possible
        if (!forceSimple) {
            instance = extendedInstances.get(tapePath);
        }

        // Fallback to simple instances if no extended instance is found
        if (instance == null) {
            instance = simpleInstances.get(tapePath);
        }

        if (instance == null) {
            OS currentOs = getCurrentOs();

            if (currentOs == null) {
                throw new TapeException(TapeException.ErrorCode.NO_NATIVE_IMPL_AVAILABLE, "No bindings for accessing the tape drive are available for this OS, as the OS could not be detected");
            }

            Optional<ServiceLoader.Provider<SimpleTapeDrive>> drive = ServiceLoader
                    .load(SimpleTapeDrive.class)
                    .stream()
                    .filter(
                            d -> {
                                try {
                                    return d.type().getField("TARGET_OS").get(OS.class) == currentOs;
                                } catch (Throwable e) {
                                    String msg = e.getMessage();
                                    if (e.getCause() != null && e.getCause().getMessage() != null) {
                                        msg = e.getCause().getMessage();
                                    }
                                    LOGGER.warn("Could not evaluate native binding implementation "+d.type().getName()+" as runtime candidate: "+msg);
                                    return false;
                                }
                            }
                    )
                    .filter(d -> {
                        if (forceSimple) {
                            return !(d instanceof ExtendedTapeDrive);
                        } else {
                            return true;
                        }
                    }
                    )
                    .findFirst();

            instance = returnInstanceIfExistsOrFail(drive, tapePath);
        }

        return instance;
    }

    /**
     * Returns a plattform-specif implementation of the ExtendedTapeDrive class
     * @param tapePath Path to the tape file to access the tape (e.g. \\.\TAPE0 on windows)
     * @return Plattform-specif implementation of the SimpleTapeDrive class
     * @throws TapeException If the drive cannot be correctly accessed the underlying system APIs, or if no implementation for the currently running system exists
     */
    public static synchronized ExtendedTapeDrive getExtendedTapeDriveInstance(String tapePath) throws TapeException {
        ExtendedTapeDrive instance = extendedInstances.get(tapePath);

        if (instance == null) {
            OS currentOs = getCurrentOs();

            if (currentOs == null) {
                throw new TapeException(TapeException.ErrorCode.NO_NATIVE_IMPL_AVAILABLE, "No bindings for accessing the tape drive are available for this OS, as the OS could not be detected");
            }

            Optional<ServiceLoader.Provider<ExtendedTapeDrive>> drive = ServiceLoader
                    .load(ExtendedTapeDrive.class)
                    .stream()
                    .filter(d -> d.get().getTargetOs() == currentOs)
                    .findFirst();

            instance = returnInstanceIfExistsOrFail(drive, tapePath);
        }

        return instance;
    }

    private static<T extends SimpleTapeDrive> T returnInstanceIfExistsOrFail(Optional<ServiceLoader.Provider<T>> drive, String tapePath) throws TapeException {
        T instance = null;

        if (drive.isPresent()) {
            instance = drive.get().get();
            instance.setTapeDriveFile(tapePath);
        }

        if (instance != null) {
            simpleInstances.put(tapePath, instance);
        } else {
            throw new TapeException(TapeException.ErrorCode.NO_NATIVE_IMPL_AVAILABLE, "No bindings for accessing the tape drive are available for this OS ("+System.getProperty("os.name")+")");
        }

        return instance;
    }

    public static OS getCurrentOs() {
        if (isWindowsOs()) {
            return OS.WINDOWS;
        }

        if (isLinuxOs()) {
            return OS.LINUX;
        }

        if (isMacOs()) {
            return OS.MACOS;
        }

        return null;
    }

    public static boolean isWindowsOs() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    public static boolean isLinuxOs() {
        return System.getProperty("os.name").toLowerCase().contains("linux");
    }

    public static boolean isMacOs() {
        return System.getProperty("os.name").toLowerCase().contains("mac");
    }
}
