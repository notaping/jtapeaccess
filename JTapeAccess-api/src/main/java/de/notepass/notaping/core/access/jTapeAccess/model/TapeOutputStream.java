package de.notepass.notaping.core.access.jTapeAccess.model;

import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Extremely minimalistic OutputStream which uses the tape drive interface for communications
 */
public class TapeOutputStream extends WrittenBytesAwareOutputStream {
    protected final SimpleTapeDrive tapeDevice;

    public TapeOutputStream(SimpleTapeDrive td) {
        tapeDevice = td;
    }

    @Override
    public int writeA(int i) throws IOException {
        return tapeDevice.writeBytes(new byte[]{(byte)i});
    }

    @Override
    public int flushA() throws IOException {
        tapeDevice.flushCaches();
        return 0;
    }

    @Override
    public int writeA(byte[] val, int off, int len) throws IOException {
        return tapeDevice.writeBytes(val, off, len);
    }

    @Override
    public int writeA(byte[] val) throws IOException {
        return tapeDevice.writeBytes(val);
    }

    @Override
    public void flush() throws IOException {
        tapeDevice.flushCaches();
    }

    @Override
    public int getExpectedNextFlushSize() {
        return 0;
    }
}
