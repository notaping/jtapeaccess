package de.notepass.notaping.core.access.jTapeAccess.impl.stream;

import de.notepass.notaping.core.access.jTapeAccess.model.*;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class StreamBasedTapeDrive implements SimpleTapeDrive {
    private TapeMetadataEncounteredListener listener;
    private STREAM_MODE streamMode;

    private boolean virtualTapeLoaded = false;

    private InputStream is;
    private OutputStream os;

    private BlockSizeCorrectingTapeOutputStream<TapeOutputStream> userFacingOs;
    private BlockSizeCorrectingTapeInputStream<TapeInputStream> userFacingIs;

    private long position = 0;
    private int blockSize = 65536;

    public enum STREAM_MODE {
        READ, WRITE
    }

    public static StreamBasedTapeDrive createForStream(OutputStream destination) throws IOException {
        StreamBasedTapeDrive std = new StreamBasedTapeDrive();
        std.setStream(destination);
        std.load();

        return std;
    }

    public static StreamBasedTapeDrive createForStream(InputStream source) throws IOException {
        StreamBasedTapeDrive std = new StreamBasedTapeDrive();
        std.setStream(source);
        std.load();

        return std;
    }

    public void load() throws IOException {
        if (streamMode == null) {
            throw new IllegalStateException("load() can only be called after setFileMode()");
        }

        if (virtualTapeLoaded) {
            throw new IllegalStateException("load() cannot be called on a loaded object. Call unload() first");
        }

        if (is == null && os == null) {
            throw new IllegalStateException("load() can only be called if the operating stream was set");
        }

        if (streamMode == STREAM_MODE.READ) {
            TapeInputStream tis = new TapeInputStream(this);
            userFacingIs = new BlockSizeCorrectingTapeInputStream<>(tis);

            virtualTapeLoaded = true;
        } else if (streamMode == STREAM_MODE.WRITE) {
            TapeOutputStream tos = new TapeOutputStream(this);
            userFacingOs = new BlockSizeCorrectingTapeOutputStream<>(tos);

            virtualTapeLoaded = true;
        } else {
            throw new IllegalArgumentException("Unknown streamMode " + streamMode + ". Valid are READ and WRITE");
        }
    }

    /**
     * Removes references to internal streams but doesn't close them<br/>
     * While this guarantees that streams passed via {@link #setStream(InputStream)} / {@link #setStream(OutputStream)}
     * / {@link #createForStream(InputStream)} / {@link #createForStream(OutputStream)}
     * are not closed, it also means that this could lead to dangling opened streams which result in problems at other parts
     */
    public void unloadWithDanglingStreams() {
        // Close streams
        if (userFacingIs != null) {
            // Remove references. Delegates should be closed by parents (Except for the file stream, as that is handled
            // in here and not in the user-facing stream chain)
            userFacingIs = null;
            is = null;
        }

        if (userFacingOs != null) {
            // Remove references. Delegates should be closed by parents (Except for the file stream, as that is handled
            // in here and not in the user-facing stream chain)
            userFacingOs = null;
            os = null;
        }

        // Reset fields
        streamMode = null;
        position = 0;
        blockSize = 65536;
        virtualTapeLoaded = false;
    }

    /**
     * Closes the internal wrapper streams (which may close the delegating streams)<br/>
     * If you want to avoid closing internal and delegating streams, call {@link #unloadWithDanglingStreams()}
     * @throws IOException
     */
    public void unload() throws IOException {
        // Close streams
        if (userFacingIs != null) {
            userFacingIs.close();
        }

        if (userFacingOs != null) {
            userFacingOs.close();
        }

        // Call to remove references, streams have been closed
        unloadWithDanglingStreams();
    }

    @Override
    public void setTapeMetadataListener(TapeMetadataEncounteredListener listener) {
        this.listener = listener;
    }

    // No target OS, only manual instantiation
    @Override
    public OS getTargetOs() {
        return null;
    }

    @Override
    public boolean isReadable() throws TapeException {
        return is != null && streamMode == STREAM_MODE.READ;
    }

    @Override
    public boolean isWritable() throws TapeException {
        return os != null && streamMode == STREAM_MODE.WRITE;
    }

    @Override
    public long getTapeBlockPosition() throws TapeException {
        return position / blockSize;
    }

    @Override
    public void setTapeBlockPosition(long pos) throws TapeException {
        setPosition(pos * blockSize);
    }

    private void setPosition(long targetPos) throws TapeException {
        if (streamMode != STREAM_MODE.READ) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Setting tape position is only supported in READ mode");
        }

        try {
            position += is.skip(targetPos - position);
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not skip forward "+(targetPos - position)+" bytes", e);
        }

        if (position != targetPos) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Current position is not expected position. Expected:"+targetPos+"; Actual: "+position);
        }

        invalidateInputStream();
    }

    @Override
    public void forwardFilemarks(int amount) throws TapeException {
        throw new IllegalStateException("Stream based volumes do not support filemark skipping");
    }

    @Override
    public void reverseFilemarks(int amount) throws TapeException {
        throw new IllegalStateException("Stream based volumes do not support filemark skipping");
    }

    @Override
    public DriveCapabilities[] getDriveCapabilities() throws TapeException {
        return new DriveCapabilities[]{};
    }

    @Override
    public void enableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {
        // NOOP
    }

    @Override
    public void disableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {
        // NOOP
    }

    protected int parentRead(byte[] buffer) throws IOException {
        return parentRead(buffer, 0, buffer.length);
    }

    protected int parentRead(byte[] buffer, int offset, int numBytes) throws IOException {
        int read = is.read(buffer, offset, numBytes);
        if (read < 0) {
            read = 0;
        }

        return read;
    }

    /**
     * Sets the block size used for writing to and reading from the medium
     * A default value of 64KiB will be set on initialization.<br/>
     * After this property is set all read and write operations must be done with a multiple of the given block size. So make sure to use correcting streams!
     * {@link BlockSizeCorrectingTapeInputStream} and {@link BlockSizeCorrectingTapeOutputStream})<br/>
     * <b>Please note:</b> Do not use a block size of 0!<br/>
     * <b>Additional info for file based storage:</b> The block size has mostly no impact here. The only thing it changes
     * is the amount of zeros written to fill the remaining bytes of the last block. The default value is still 64k, to be
     * equal with real tape volumes, but can be set to 1 or 2M if problems with read/write performance occur. The generated
     * data can then still be dd to a tape with bs=64k (If you choose a multiple of 64k for the buffer size) and an eof mark
     * can be written afterwards with mt eof 1.
     *
     * @param blockSizeBytes
     */
    @Override
    public void setMediumBlockSize(int blockSizeBytes) throws TapeException {
        if (is != null || os != null) {
            throw new IllegalStateException("BlockSize can only be set before calling load()");
        }

        blockSize = blockSizeBytes;
    }

    @Override
    public int readBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        if (streamMode != STREAM_MODE.READ) {
            throw new IllegalStateException("readBytes() can only be called if in READ mode");
        }

        if (is == null) {
            throw new IllegalStateException("readBytes() can only be called after calling load()");
        }

        try {
            int read = parentRead(buffer, offset, numBytes);
            position += read;
            return read;
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Error in parent read() function", e);
        }
    }

    @Override
    public int writeBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        if (streamMode != STREAM_MODE.WRITE) {
            throw new IllegalStateException("writeBytes() can only be called if in WRITE mode");
        }

        if (os == null) {
            throw new IllegalStateException("writeBytes() can only be called after calling load()");
        }

        try {
            os.write(buffer, offset, numBytes);
            position += numBytes;
            return numBytes;
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Error in parent read() function", e);
        }
    }

    @Override
    public int readBytes(byte[] buffer) throws TapeException {
        return readBytes(buffer, 0, buffer.length);
    }

    @Override
    public int writeBytes(byte[] buffer) throws TapeException {
        return writeBytes(buffer, 0, buffer.length);
    }

    @Override
    public void writeFilemarks(int amount) throws TapeException {
        throw new IllegalStateException("File based volumes do not support filemarks");
    }

    @Override
    public WrittenBytesAwareOutputStream getOutputStream() throws TapeException {
        return userFacingOs;
    }

    @Override
    public InputStream getInputStream() throws TapeException {
        return userFacingIs;
    }

    @Override
    public WrittenBytesAwareOutputStream getRawOutputStream() throws TapeException {
        return userFacingOs.getDelegate();
    }

    @Override
    public InputStream getRawInputStream() throws TapeException {
        return userFacingIs.getDelegate();
    }

    @Override
    public void flushCaches() throws TapeException {

    }

    @Override
    public boolean isMediaPresent() throws TapeException {
        return virtualTapeLoaded;
    }

    @Override
    public TapeInfo getTapeInfo() throws TapeException {
        return new TapeInfo(Long.MAX_VALUE, position, Long.MAX_VALUE, streamMode == STREAM_MODE.READ, blockSize);
    }

    @Override
    public void setTapeDriveFile(String tapeDriveFile) throws TapeException {
        throw new IllegalArgumentException("setTapeDriveFile() cannot be used in StreamBasedTapeDrive. Please use setStream()");
    }

    public void setStream(InputStream source) {
        is = source;
        streamMode = STREAM_MODE.READ;
    }

    public void setStream(OutputStream destination) {
        os = destination;
        streamMode = STREAM_MODE.WRITE;
    }

    @Override
    public String getTapeDriveFile() {
        return null;
    }

    public STREAM_MODE getStreamMode() {
        return streamMode;
    }

    public void setStreamMode(STREAM_MODE streamMode) {
        this.streamMode = streamMode;
    }

    private void invalidateInputStream() {
        if (userFacingIs != null) {
            userFacingIs.invalidate();
        }
    }

    public int getBlockSize() {
        return blockSize;
    }
}
