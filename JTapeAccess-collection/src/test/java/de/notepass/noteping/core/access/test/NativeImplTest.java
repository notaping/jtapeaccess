package de.notepass.noteping.core.access.test;

import de.notepass.notaping.core.access.jTapeAccess.SimpleTapeDriveUtils;
import de.notepass.notaping.core.access.jTapeAccess.impl.linux.ExtendedTapeDriveBindingsLinux;
import de.notepass.notaping.core.access.jTapeAccess.impl.windows.ExtendedTapeDriveWindowsBindings;
import de.notepass.notaping.core.access.jTapeAccess.model.ExtendedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.OS;
import de.notepass.notaping.core.access.jTapeAccess.model.TapeException;
import de.notepass.notaping.core.access.jTapeAccess.model.TapeInfo;

import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;
import java.util.Random;
import java.util.ServiceLoader;

@RunWith(JUnitPlatform.class)
public class NativeImplTest {
    static Random generateRandom = new Random(123123123);
    static Random checkRandom = new Random(123123123);
    static OS os = SimpleTapeDriveUtils.getCurrentOs();
    static ExtendedTapeDrive tapeDrive;
    static String tapeDriveFile;
    static boolean prepared = false;

    // TODO: [Low] Figure out why tf the service loader doesn't work when using surefire
    // TODO: [Low] Remove this if surefire finally does it's job correctly
    public static synchronized ExtendedTapeDrive getExtendedTapeDriveInstance(String tapePath) throws TapeException {
        ExtendedTapeDrive bindigs;

        switch (os) {
            case LINUX:
                bindigs = new ExtendedTapeDriveBindingsLinux();
                break;
            case WINDOWS:
                bindigs = new ExtendedTapeDriveWindowsBindings();
                break;
            default:
                throw new TapeException(TapeException.ErrorCode.NO_NATIVE_IMPL_AVAILABLE, "No bindings for accessing the tape drive are available for this OS, as the OS could not be detected");
        }

        bindigs.setTapeDriveFile(tapePath);

        return bindigs;
    }

    @BeforeAll
    public static void prepare() throws TapeException {
        if (prepared) {
            return;
        }

        tapeDriveFile = System.getProperty("notaping.test.tape");
        if (tapeDriveFile == null || tapeDriveFile.isEmpty()) {
            fail("Please set JVM param notaping.test.tape to point to the tape drive file (-Dnotaping.test.tape=<Path>)");
        }
        tapeDrive = getExtendedTapeDriveInstance(tapeDriveFile);

        System.out.println("=============== STARTING TEST FOR OS "+os+" AND TAPE DEVICE "+tapeDriveFile+" ===============");

        prepared = true;
    }

    @AfterAll
    public static void end() {
        System.out.println("=============== END TEST FOR OS "+os+" AND TAPE DEVICE "+tapeDriveFile+" ===============");
    }

    public void assertReadAccess() throws TapeException {
        if (!tapeDrive.isMediaPresent()) {
            fail("No media present- Please insert media into tape device "+tapeDrive);
        }
    }

    public void assertWriteAccess() throws TapeException {
        assertReadAccess();

        if (tapeDrive.getTapeInfo().isWriteProtected()) {
            fail("Media in tape device "+tapeDrive+" is write protected. Please remove write protection.");
        }
    }

    @Test
    @Timeout(60000)
    public void testEjectAndLoad() throws TapeException {

        assertReadAccess();

        System.out.println("Ejecting tape");
        long startTime = System.currentTimeMillis();
        tapeDrive.ejectTape();
        long timeTaken = System.currentTimeMillis() - startTime;

        if (timeTaken < 1000) {
            // Sanity check: Tape devices take time to unload
            fail("Tape unloaded unrealistically fast");
        }

        if (tapeDrive.isMediaPresent()) {
            // Sanity check: Tape removed?
            fail("Tape drive indicates that there is still a tape in the device");
        }

        System.out.println("Loading tape");
        startTime = System.currentTimeMillis();
        tapeDrive.loadTape();
        timeTaken = System.currentTimeMillis() - startTime;

        if (timeTaken < 1000) {
            // Sanity check: Tape devices take time to unload
            fail("Tape loaded unrealistically fast");
        }

        if (!tapeDrive.isMediaPresent()) {
            // Sanity check: Tape removed?
            fail("Tape drive indicates that there is not a tape in the device");
        }
    }

    @ParameterizedTest
    @Timeout(300000)
    @ValueSource(ints = {65536, 32768})
    public void testWriteReadSeekPosition(int blockSize) throws TapeException {
        final int NUM_WRITE_BLOCKS = 100;

        assertWriteAccess();

        TapeInfo ti = tapeDrive.getTapeInfo();
        if (ti.getBlockSizeBytes() != blockSize) {
            System.out.println("Switching device block site from "+ti.getBlockSizeBytes()+" to "+blockSize);
            tapeDrive.setMediumBlockSize(blockSize);
            ti = tapeDrive.getTapeInfo();
            if (ti.getBlockSizeBytes() != blockSize) {
                fail("Could not change device block size. Requested "+blockSize+", but got "+ti.getBlockSizeBytes());
            }
        }

        final long startBlock = tapeDrive.getTapeBlockPosition();
        System.out.println("Starting to write "+NUM_WRITE_BLOCKS+" blocks of data");
        byte[] writeBuf = new byte[blockSize];
        byte[] readBuf = new byte[blockSize];

        for (int i = 0; i < NUM_WRITE_BLOCKS; i++) {
            generateRandom.nextBytes(writeBuf);
            tapeDrive.writeBytes(writeBuf);
        }
        tapeDrive.flushCaches();

        long writtenBlocks = tapeDrive.getTapeBlockPosition() - startBlock;
        assertEquals(NUM_WRITE_BLOCKS, writtenBlocks, "Tape device wrote wrong number of blocks");

        System.out.println("Setting device to start position "+startBlock);
        tapeDrive.setTapeBlockPosition(startBlock);
        assertEquals(startBlock, tapeDrive.getTapeBlockPosition(), "Device did not seekt to expected position");

        System.out.println("Checking if written data is valid");
        for (int i = 0; i < NUM_WRITE_BLOCKS; i++) {
            checkRandom.nextBytes(writeBuf);
            tapeDrive.readBytes(readBuf);
            assertArrayEquals(writeBuf, readBuf, "Data for written block "+i+" of "+NUM_WRITE_BLOCKS+" is wrong");
        }
    }

    @Test
    @Timeout(500000)
    public void testFilemarks() throws TapeException {

        // Create local randoms, as the check random needs to be recreated later
        Random generateRandom = new Random(123123123);
        Random checkRandom = new Random(123123123);

        final int NUM_WRITE_BLOCKS_AFTER_FILEMARK = 100;
        final int NUM_WRITE_BLOCKS_BEFORE_FILEMARK = 10;
        final int blockSize = (int) tapeDrive.getTapeInfo().getBlockSizeBytes();

        assertWriteAccess();

        final long startBlockPos = tapeDrive.getTapeBlockPosition();

        System.out.println("Writing "+NUM_WRITE_BLOCKS_BEFORE_FILEMARK+" blocks before writing filemark");
        byte[] writeBuf = new byte[blockSize];
        byte[] readBuf = new byte[blockSize];
        for (int i = 0; i < NUM_WRITE_BLOCKS_BEFORE_FILEMARK; i++) {
            int written = tapeDrive.writeBytes(writeBuf);
            assertEquals(writeBuf.length, written, "Tape device reported wrong amount of bytes written");
        }
        tapeDrive.flushCaches();

        long blocksWritten = tapeDrive.getTapeBlockPosition() - startBlockPos;
        assertEquals(NUM_WRITE_BLOCKS_BEFORE_FILEMARK, blocksWritten, "Tape device did not write the expected amount of blocks");

        System.out.println("Writing filemark");
        tapeDrive.writeFilemarks(1);
        long filemarkPos = tapeDrive.getTapeBlockPosition();

        System.out.println("Writing "+NUM_WRITE_BLOCKS_AFTER_FILEMARK+" after writing filemark");
        for (int i = 0; i < NUM_WRITE_BLOCKS_AFTER_FILEMARK; i++) {
            generateRandom.nextBytes(writeBuf);
            int written = tapeDrive.writeBytes(writeBuf);
            assertEquals(writeBuf.length, written, "Tape device reported wrong amount of bytes written");
        }
        tapeDrive.flushCaches();

        blocksWritten = tapeDrive.getTapeBlockPosition() - startBlockPos;

        // +1 for block used by EOF
        assertEquals(NUM_WRITE_BLOCKS_BEFORE_FILEMARK + NUM_WRITE_BLOCKS_AFTER_FILEMARK + 1, blocksWritten, "Tape device did not write the expected amount of blocks");

        System.out.println("Reversing one filemark");
        tapeDrive.reverseFilemarks(1);
        assertEquals(filemarkPos, tapeDrive.getTapeBlockPosition(), "Did not set position to cached filemark position");

        System.out.println("Checking data");
        for (int i = 0; i < NUM_WRITE_BLOCKS_AFTER_FILEMARK; i++) {
            checkRandom.nextBytes(writeBuf);
            tapeDrive.readBytes(readBuf);
            assertArrayEquals(writeBuf, readBuf, "Invalid data in read block #"+i);
        }

        System.out.println("Jumping in front of filemark");
        tapeDrive.setTapeBlockPosition(startBlockPos);
        assertEquals(startBlockPos, tapeDrive.getTapeBlockPosition(), "Did not jump to expected position");

        System.out.println("Jumping forward to filemark");
        tapeDrive.forwardFilemarks(1);
        assertEquals(filemarkPos, tapeDrive.getTapeBlockPosition(), "Did not set position to cached filemark position");

        checkRandom = new Random(123123123);
        System.out.println("Checking data");
        for (int i = 0; i < NUM_WRITE_BLOCKS_AFTER_FILEMARK; i++) {
            checkRandom.nextBytes(writeBuf);
            tapeDrive.readBytes(readBuf);
            assertArrayEquals(writeBuf, readBuf, "Invalid data in read block #"+i);
        }
    }
}
