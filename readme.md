# JTapeAccess
JTapeAccess is a java library for cross-plattform tape drive access. It allows for
reading and writing to an inserted tape, as well as for configuring the tape drive
itself.

## Using the API
The API consists of basically only one interface: The SimpleTapeDrive-interface. This
contains all methods currently available for tape access. It can be used to check if a
drive is ready, if a tape is inserted, load a tape, eject a tape, perform read and write
operations on a tape, as well as configure capabilities of the drive (e.g. enabling compression).
Additionally, errors generated by the native tape access API are wrapped into normal Java-Exceptions
with an "error code", which can be used to determinate the fault.

To access a platform-specific implementation the SimpleTapeDriveUtils-class can be used.
Just call the "getTapeDriveInstance(String)" method.  

An example use of the library can be found under src/test/java/ExampleClient.java

## Creating new bindings
To create a new binding for a system, just implement the "SimpleTapeDrive" interface. To access
native APIs, JNA is available in the dependency tree. if you want to merge your changes with this
repo, please also make sure that the SimpleTapeDriveUtils#getTapeDriveInstance(String) method is
modified to return yu implementation as needed. As this library is licenced under the MIT licence,
you are not required to make the source code available nor to merge any of your changes into this
repo - It would be nice tho.

## Currently available implementations
Currently, only bindings for windows exist. These bindings, while only testet with Windows 10,
should be backwards-compatible to Windows XP (at least according to the Microsoft docs).  
Linux bindings are planned to be added until the 1.0 release. As I do not have access to an
OSX device, no MacOS-implementation is currently planned.

## Current state of the library
The library is still in a pretty early state and considered beta software - Errors and bugs
are to be expected. If you find something open up an issue or create a pull request.  

For more information please see the [project state Wiki page](wikis/project-state)