package de.notepass.notaping.core.access.jTapeAccess.impl.windows;

import de.notepass.notaping.core.access.jTapeAccess.model.TapeException;

import static com.sun.jna.platform.win32.WinError.*;

public class ExceptionMapper {
    /**
     * Maps windows API error codes to TapeException codes (including messages)
     * @param errorCode Windows error code
     * @return Throwable exception
     */
    public static TapeException map(long errorCode) {
        switch ((int) errorCode) {
            case 0:
                break;
            case ERROR_BEGINNING_OF_MEDIA:
                return new TapeException(TapeException.ErrorCode.INVALID_TAPE_POSITION, "An attempt to access data before the beginning-of-medium marker failed.");
            case ERROR_BUS_RESET:
                return new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "A reset condition was detected on the bus.");
            case ERROR_DEVICE_NOT_PARTITIONED:
                return new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "The partition information could not be found when a tape was being loaded.");
            case ERROR_INVALID_BLOCK_LENGTH:
                return new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "The block size is incorrect on a new tape in a multivolume partition.");
            case ERROR_MEDIA_CHANGED:
                break;
                //return new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "The tape that was in the drive has been replaced or removed.");
            case ERROR_PARTITION_FAILURE:
                return new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "The tape could not be partitioned.");
            case ERROR_END_OF_MEDIA:
                break;
                //return new TapeException(TapeException.ErrorCode.NO_SPACE_LEFT, "The end-of-tape marker was reached during an operation.");
            case ERROR_FILEMARK_DETECTED:
                break;
                //    return new TapeException(TapeException.ErrorCode.INVALID_TAPE_POSITION, "A filemark was reached during an operation.");
            case ERROR_NO_DATA_DETECTED:
                return new TapeException(TapeException.ErrorCode.END_OF_DATA_REACHED, "The end-of-data marker was reached during an operation.");
            case ERROR_NO_MEDIA_IN_DRIVE:
                return new TapeException(TapeException.ErrorCode.NO_TAPE_IN_DRIVE, "There is no media in the drive.");
            case ERROR_NOT_SUPPORTED:
                return new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "The tape driver does not support a requested function.");
            case ERROR_SETMARK_DETECTED:
                return new TapeException(TapeException.ErrorCode.INVALID_TAPE_POSITION, "A setmark was reached during an operation.");
            case ERROR_UNABLE_TO_LOCK_MEDIA:
                return new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "An attempt to lock the ejection mechanism failed.");
            case ERROR_UNABLE_TO_UNLOAD_MEDIA:
                return new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "An attempt to unload the tape failed.");
            case ERROR_WRITE_PROTECT:
                return new TapeException(TapeException.ErrorCode.DEVICE_NOT_WRITABLE, "The media is write protected.");
            default:
                return new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Unknown error in native windows implementation occurred. Error code: "+errorCode+". Please check https://docs.microsoft.com/en-us/windows/win32/debug/system-error-codes for more information.");
        }

        return null;
    }
}
