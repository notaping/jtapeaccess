package de.notepass.notaping.core.access.jTapeAccess.impl.windows;

import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinError;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.ptr.IntByReference;
import de.notepass.notaping.core.access.jTapeAccess.impl.NoopTapeMetaDataListener;
import de.notepass.notaping.core.access.jTapeAccess.model.*;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static de.notepass.notaping.core.access.jTapeAccess.impl.windows.Kernel32WithTape.*;

public class ExtendedTapeDriveWindowsBindings implements ExtendedTapeDrive {
    private final Kernel32WithTape kernel32 = Kernel32WithTape.INSTANCE;
    private WinNT.HANDLE tapeHandle;
    public ParsedTapeDriveCapabilities tapeDriveCapabilities;
    private String tapeDriveFile;
    private BlockSizeCorrectingTapeOutputStream correctingTapeOutputStreamInstance;
    private TapeOutputStream tapeOutputStreamInstance;
    private BlockSizeCorrectingTapeInputStream correctingTapeInputStreamInstance;
    private TapeInputStream tapeInputStreamInstance;
    private TapeMetadataEncounteredListener tapeMetadataListener = new NoopTapeMetaDataListener();
    private static final Logger LOGGER = LoggerFactory.getLogger(ExtendedTapeDriveWindowsBindings.class);

    public static final OS TARGET_OS = OS.WINDOWS;

    public ExtendedTapeDriveWindowsBindings() throws TapeException {
        this(null);
    }

    public ExtendedTapeDriveWindowsBindings(String tapeDriveFile) throws TapeException {
        if (tapeDriveFile != null) {
            setTapeDriveFile(tapeDriveFile);
        }
    }

    @Override
    public void setTapeMetadataListener(TapeMetadataEncounteredListener listener) {
        tapeMetadataListener = listener;

        if (listener == null) {
            tapeMetadataListener = new NoopTapeMetaDataListener();
        }
    }

    @Override
    public OS getTargetOs() {
        return OS.WINDOWS;
    }

    @Override
    public void loadTape() throws TapeException {
        checkError(kernel32.PrepareTape(tapeHandle, new WinDef.DWORD(TAPE_LOAD), new WinDef.BOOL(false)));
        invalidateInputStream();
    }

    @Override
    public void ejectTape() throws TapeException {
        checkError(kernel32.PrepareTape(tapeHandle, new WinDef.DWORD(TAPE_UNLOAD), new WinDef.BOOL(false)));
        invalidateInputStream();
    }

    @Override
    public boolean isReadable() {
        return isMediaPresent();
    }

    @Override
    public boolean isWritable() throws TapeException {
        return getTapeInfo().isWriteProtected();
    }

    @Override
    public long getTapeBlockPosition() throws TapeException {
        WinDef.DWORDByReference lpdwPartition = new WinDef.DWORDByReference(new DWORD(0));
        WinDef.DWORDByReference lpdwOffsetLow = new WinDef.DWORDByReference();
        WinDef.DWORDByReference lpdwOffsetHigh = new WinDef.DWORDByReference();
        checkError(kernel32.GetTapePosition(tapeHandle, new WinDef.DWORD(TAPE_ABSOLUTE_POSITION), lpdwPartition, lpdwOffsetLow, lpdwOffsetHigh));

        return (lpdwOffsetHigh.getValue().longValue() << 32) | lpdwOffsetLow.getValue().longValue();
    }

    @Override
    public void setTapeBlockPosition(long position) throws TapeException {
        if (position != 0) {
            long low = position & 0x00000000FFFFFFFF;
            long high = position >> 32;
            // Using TAPE_LOGICAL_BLOCK instead of TAPE_ABSOLUTE_BLOCK based on https://www.ibm.com/mysupport/s/question/0D50z00006LL51H/problem-with-settapeposition-and-ibm-ult3580hh5-under-windows-2008-r2
            checkError(kernel32.SetTapePosition(tapeHandle, new WinDef.DWORD(TAPE_LOGICAL_BLOCK), new WinDef.DWORD(0L), new WinDef.DWORD(low), new WinDef.DWORD(high), new WinDef.BOOL(false)));
        } else {
            checkError(kernel32.SetTapePosition(tapeHandle, new WinDef.DWORD(TAPE_REWIND), new WinDef.DWORD(0L), new WinDef.DWORD(0), new WinDef.DWORD(0), new WinDef.BOOL(false)));
        }

        invalidateInputStream();
    }

    @Override
    public void forwardFilemarks(int amount) throws TapeException {
        checkError(kernel32.SetTapePosition(tapeHandle, new WinDef.DWORD(TAPE_SPACE_FILEMARKS), new WinDef.DWORD(0L), new WinDef.DWORD(amount), new WinDef.DWORD(0), new WinDef.BOOL(false)));
        invalidateInputStream();
    }

    @Override
    public void reverseFilemarks(int amount) throws TapeException {
        // Multiply by -1 to move back the given amount of filemarks
        amount *= -1;

        checkError(kernel32.SetTapePosition(tapeHandle, new WinDef.DWORD(TAPE_SPACE_FILEMARKS), new WinDef.DWORD(0L), new WinDef.DWORD(amount), new WinDef.DWORD(0), new WinDef.BOOL(false)));

        // Move forward one block to position after filemark
        setTapeBlockPosition(getTapeBlockPosition() + 1);

        invalidateInputStream();
    }

    @Override
    public DriveCapabilities[] getDriveCapabilities() {
        boolean ecc = tapeDriveCapabilities.TAPE_DRIVE_ECC;
        boolean compression = tapeDriveCapabilities.TAPE_DRIVE_COMPRESSION;
        boolean padding = tapeDriveCapabilities.TAPE_DRIVE_PADDING;
        List<DriveCapabilities> driveCapabilities = new ArrayList<>(5);
        if (ecc) {
            driveCapabilities.add(DriveCapabilities.ECC);
        }

        if (compression) {
            driveCapabilities.add(DriveCapabilities.COMPRESSION);
        }

        if (padding) {
            driveCapabilities.add(DriveCapabilities.DATA_PADDING);
        }

        return driveCapabilities.toArray(new DriveCapabilities[0]);
    }

    @Override
    public void enableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {
        Kernel32WithTape.TAPE_SET_DRIVE_PARAMETERS.ByReference driveParameters = new Kernel32WithTape.TAPE_SET_DRIVE_PARAMETERS.ByReference();
        for (DriveCapabilities driveCapability : driveCapabilities) {
            switch (driveCapability) {
                case ECC:
                    driveParameters.ECC = new BYTE(0xFF);
                    break;
                case COMPRESSION:
                    driveParameters.Compression =  new BYTE(0xFF);
                    break;
                case DATA_PADDING:
                    driveParameters.DataPadding =  new BYTE(0xFF);
            }
        }


        checkError(kernel32.SetTapeParameters(tapeHandle, new WinDef.DWORD(SET_TAPE_DRIVE_INFORMATION), driveParameters));
    }

    @Override
    public void disableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {
        Kernel32WithTape.TAPE_SET_DRIVE_PARAMETERS.ByReference driveParameters = new Kernel32WithTape.TAPE_SET_DRIVE_PARAMETERS.ByReference();
        for (DriveCapabilities driveCapability : driveCapabilities) {
            switch (driveCapability) {
                case ECC:
                    driveParameters.ECC =  new BYTE(0);
                    break;
                case COMPRESSION:
                    driveParameters.Compression =  new BYTE(0);
                    break;
                case DATA_PADDING:
                    driveParameters.DataPadding =  new BYTE(0);
            }
        }

        checkError(kernel32.SetTapeParameters(tapeHandle, new DWORD(SET_TAPE_DRIVE_INFORMATION), driveParameters));
    }

    @Override
    public void setMediumBlockSize(int blockSizeBytes) throws TapeException {
        Kernel32WithTape.TAPE_SET_MEDIA_PARAMETERS.ByReference mediaParameters = new Kernel32WithTape.TAPE_SET_MEDIA_PARAMETERS.ByReference();

        mediaParameters.BlockSize = new DWORD(blockSizeBytes);

        checkError(kernel32.SetTapeParameters(tapeHandle, new WinDef.DWORD(SET_TAPE_MEDIA_INFORMATION), mediaParameters));
    }

    @Override
    public int readBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        if (offset == 0 && buffer.length == numBytes) {
            return readBytes(buffer);
        }

        byte[] subArray = new byte[numBytes];

        int readBytes = readBytes(subArray);
        System.arraycopy(subArray, 0, buffer, offset, numBytes);

        return readBytes;
    }

    @Override
    public int writeBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        byte[] rangedCopy = Arrays.copyOfRange(buffer, offset, numBytes + offset);
        return writeBytes(rangedCopy);
    }

    @Override
    public int readBytes(byte[] buffer) throws TapeException {
        if (buffer.length == 0) {
            return 0;
        }

        DWORDByReference readBytesDw = new DWORDByReference(new DWORD(-1L));
        IntByReference readBytes = new IntByReference(-1);
        try {
            checkError(kernel32.ReadFile(tapeHandle, buffer, buffer.length, readBytes, null));
        } catch (TapeException e) {
            // Check if the error was a filemark encounter
            if (e.getErrorCode() == TapeException.ErrorCode.EOF_MARK_ENCOUNTERED) {
                tapeMetadataListener.onFilemarkEncountered();
            } else {
                throw e;
            }
        }
        return readBytes.getValue();
    }

    @Override
    public int writeBytes(byte[] buffer) throws TapeException {
        if (buffer.length % getTapeInfo().getBlockSizeBytes() != 0) {
            LOGGER.warn(
                    String.format(
                            "Trying to write data to the tape with a wrong block size. Data can only be " +
                                    "written in multiples of %dKiB, but it was tried to write %dKiB. " +
                                    "This will almost certainly fail%n",
                            (getTapeInfo().getBlockSizeBytes() / 1024),
                            (buffer.length / 1024))
            );
        }

        IntByReference writtenBytes = new IntByReference(-1);
        checkError(kernel32.WriteFile(tapeHandle, buffer, buffer.length, writtenBytes, null));
        return writtenBytes.getValue();
    }

    @Override
    public void writeFilemarks(int amount) throws TapeException {
        checkError(kernel32.WriteTapemark(tapeHandle, new WinDef.DWORD(TAPE_FILEMARKS), new WinDef.DWORD(amount), new WinDef.BOOL(false)));
    }

    @Override
    public WrittenBytesAwareOutputStream getOutputStream() throws TapeException {
        if (correctingTapeOutputStreamInstance == null) {
            // Make sure raw stream exists
            getRawOutputStream();
            correctingTapeOutputStreamInstance = new BlockSizeCorrectingTapeOutputStream(tapeOutputStreamInstance);
        }
        return correctingTapeOutputStreamInstance;
    }

    @Override
    public InputStream getInputStream() throws TapeException {
        if (correctingTapeInputStreamInstance == null) {
            // Make sure raw stream exists
            getRawInputStream();
            correctingTapeInputStreamInstance = new BlockSizeCorrectingTapeInputStream(tapeInputStreamInstance);
        }
        return correctingTapeInputStreamInstance;
    }

    @Override
    public WrittenBytesAwareOutputStream getRawOutputStream() throws TapeException {
        if (tapeOutputStreamInstance == null) {
            tapeOutputStreamInstance = new TapeOutputStream(this);
        }
        return tapeOutputStreamInstance;
    }

    @Override
    public InputStream getRawInputStream() throws TapeException {
        if (tapeInputStreamInstance == null) {
            tapeInputStreamInstance = new TapeInputStream(this);
        }
        return tapeInputStreamInstance;
    }

    @Override
    public void flushCaches() {
        //TODO [Low]: Should be done automatically
    }

    @Override
    public boolean isMediaPresent() {
        WinDef.DWORD driveStatus = kernel32.GetTapeStatus(tapeHandle);
        return driveStatus.intValue() != WinError.ERROR_NO_MEDIA_IN_DRIVE;
    }

    @Override
    public TapeInfo getTapeInfo() throws TapeException {
        TAPE_GET_MEDIA_PARAMETERS tapeInfo = getInsertedTapeInfo();
        return new TapeInfo(
                tapeInfo.Remaining.getValue(),
                tapeInfo.Capacity.getValue() - tapeInfo.Remaining.getValue(),
                tapeInfo.Capacity.getValue(),
                tapeInfo.WriteProtected.booleanValue(),
                tapeInfo.BlockSize.longValue()
        );
    }

    //@Override
    //public void partitionTape() throws TapeException {
    //    kernel32.CreateTapePartition(tapeHandle, new DWORD(TAPE_FIXED_PARTITIONS), new DWORD(1), new DWORD(-1));
    //}

    @Override
    public void setTapeDriveFile(String tapeDriveFile) throws TapeException {
        if (this.tapeDriveFile != null && !this.tapeDriveFile.isEmpty()) {
            throw new TapeException(TapeException.ErrorCode.CLIENT_REINITIALIZATION_NOT_SUPPORTED, "Tried to change tape drive backing file from "+this.tapeDriveFile+" to "+tapeDriveFile);
        }

        this.tapeDriveFile = tapeDriveFile;
        this.tapeDriveFile = tapeDriveFile;

        tapeHandle = kernel32.CreateFile(tapeDriveFile,
                WinNT.GENERIC_READ | WinNT.GENERIC_WRITE, /* Read and write access for tape drive */
                0, /* Do not allow other processes to access the tape drive */
                null, /* Unused for tape drives */
                WinNT.OPEN_EXISTING, /* Open an existing file */
                0, /* Do not set any attributes */
                null /* Not needed */
        );

        if (tapeHandle.toString().endsWith("@0xffffffffffffffff") || tapeHandle.toString().endsWith("@0xffffffff")) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_NOT_FOUND, "Could not open tape drive " + tapeDriveFile + ". Is it connected and is the application running with administrator privileges?");
        }

        // Make sure that the tape handle is closed upon exit
        Runtime.getRuntime().addShutdownHook(new Thread(() -> kernel32.CloseHandle(tapeHandle)));

        Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS.ByReference capabilitiesRef = new Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS.ByReference();
        checkError(
                kernel32.GetTapeParameters(
                        tapeHandle,
                        new WinDef.DWORD(GET_TAPE_DRIVE_INFORMATION), // Get tape drive info instead of tape info
                        new WinDef.DWORDByReference(new WinDef.DWORD(capabilitiesRef.size())),
                        capabilitiesRef
                )
        );

        if (isMediaPresent()) {
            setMediumBlockSize(64 * 1024);
        }

        tapeDriveCapabilities = new ParsedTapeDriveCapabilities(getTapeDriveInfo());
    }

    protected Kernel32WithTape.TAPE_GET_MEDIA_PARAMETERS getInsertedTapeInfo() throws TapeException {
        Kernel32WithTape.TAPE_GET_MEDIA_PARAMETERS.ByReference mediaInfo = new Kernel32WithTape.TAPE_GET_MEDIA_PARAMETERS.ByReference();
        checkError(kernel32.GetTapeParameters(tapeHandle, new WinDef.DWORD(GET_TAPE_MEDIA_INFORMATION), new WinDef.DWORDByReference(new WinDef.DWORD(mediaInfo.size())), mediaInfo));
        return mediaInfo;
    }

    protected Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS getTapeDriveInfo() throws TapeException {
        Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS.ByReference mediaInfo = new Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS.ByReference();
        checkError(kernel32.GetTapeParameters(tapeHandle, new WinDef.DWORD(GET_TAPE_DRIVE_INFORMATION), new WinDef.DWORDByReference(new WinDef.DWORD(mediaInfo.size())), mediaInfo));
        return mediaInfo;
    }

    protected void checkError(DWORD resultCode) throws TapeException {
        TapeException mappedException = ExceptionMapper.map(resultCode.longValue());
        if (mappedException != null) {
            throw mappedException;
        }
    }

    protected void checkError(boolean result) throws TapeException {
        if (!result) {
            TapeException mappedException = ExceptionMapper.map(kernel32.GetLastError());

            if (mappedException != null) {
                throw mappedException;
            }
        }
    }

    public String getTapeDriveFile() {
        return tapeDriveFile;
    }

    private void invalidateInputStream() {
        if (correctingTapeInputStreamInstance != null) {
            correctingTapeInputStreamInstance.invalidate();
        }
    }
}
