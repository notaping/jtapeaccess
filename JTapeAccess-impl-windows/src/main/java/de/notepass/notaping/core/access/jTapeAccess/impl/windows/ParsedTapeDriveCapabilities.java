package de.notepass.notaping.core.access.jTapeAccess.impl.windows;

import java.util.StringJoiner;

public class ParsedTapeDriveCapabilities {
    // low bits
    /**
     *  The device supports hardware data compression.
     */
    public final boolean TAPE_DRIVE_COMPRESSION;

    /**
     *  The device can report if cleaning is required.
     */
    public final boolean TAPE_DRIVE_CLEAN_REQUESTS;

    /**
     *  The device supports hardware error correction.
     */
    public final boolean TAPE_DRIVE_ECC;

    /**
     *  The device physically ejects the tape on a software eject.
     */
    public final boolean TAPE_DRIVE_EJECT_MEDIA;

    /**
     *  The device performs the erase operation from the beginning-of-partition marker only.
     */
    public final boolean TAPE_DRIVE_ERASE_BOP_ONLY;

    /**
     *  The device performs a long erase operation.
     */
    public final boolean TAPE_DRIVE_ERASE_LONG;

    /**
     *  The device performs an immediate erase operation - that is, it returns when the erase operation begins.
     */
    public final boolean TAPE_DRIVE_ERASE_IMMEDIATE;

    /**
     *  The device performs a short erase operation.
     */
    public final boolean TAPE_DRIVE_ERASE_SHORT;

    /**
     *  The device creates fixed data partitions.
     */
    public final boolean TAPE_DRIVE_FIXED;

    /**
     *  The device supports fixed-length block mode.
     */
    public final boolean TAPE_DRIVE_FIXED_BLOCK;

    /**
     *  The device provides the current device-specific block address.
     */
    public final boolean TAPE_DRIVE_GET_ABSOLUTE_BLK;

    /**
     *  The device provides the current logical block address (and logical tape partition).
     */
    public final boolean TAPE_DRIVE_GET_LOGICAL_BLK;

    /**
     *  The device creates initiator-defined partitions.
     */
    public final boolean TAPE_DRIVE_INITIATOR;

    /**
     *  The device supports data padding.
     */
    public final boolean TAPE_DRIVE_PADDING;

    /**
     *  The device supports setmark reporting.
     */
    public final boolean TAPE_DRIVE_REPORT_SMKS;

    /**
     *  The device creates select data partitions.
     */
    public final boolean TAPE_DRIVE_SELECT;

    /**
     *  The device must be at the beginning of a partition before it can set compression on.
     */
    public final boolean TAPE_DRIVE_SET_CMP_BOP_ONLY;

    /**
     *  The device supports setting the end-of-medium warning size.
     */
    public final boolean TAPE_DRIVE_SET_EOT_WZ_SIZE;

    /**
     *  The device returns the maximum capacity of the tape.
     */
    public final boolean TAPE_DRIVE_TAPE_CAPACITY;

    /**
     *  The device returns the remaining capacity of the tape.
     */
    public final boolean TAPE_DRIVE_TAPE_REMAINING;

    /**
     *  The device supports variable-length block mode.
     */
    public final boolean TAPE_DRIVE_VARIABLE_BLOCK;

    /**
     *  The device returns an error if the tape is write-enabled or write-protected.
     */
    public final boolean TAPE_DRIVE_WRITE_PROTECT;

    // High bits
    /**
     *  The device moves the tape to a device-specific block address and returns as soon as the move begins.
     */
    public final boolean TAPE_DRIVE_ABS_BLK_IMMED;

    /**
     *  The device moves the tape to a device specific block address.
     */
    public final boolean TAPE_DRIVE_ABSOLUTE_BLK;

    /**
     *  The device moves the tape to the end-of-data marker in a partition.
     */
    public final boolean TAPE_DRIVE_END_OF_DATA;

    /**
     *  The device moves the tape forward (or backward) a specified number of filemarks.
     */
    public final boolean TAPE_DRIVE_FILEMARKS;

    /**
     *  The device enables and disables the device for further operations.
     */
    public final boolean TAPE_DRIVE_LOAD_UNLOAD;

    /**
     *  The device supports immediate load and unload operations.
     */
    public final boolean TAPE_DRIVE_LOAD_UNLD_IMMED;

    /**
     *  The device enables and disables the tape ejection mechanism.
     */
    public final boolean TAPE_DRIVE_LOCK_UNLOCK;

    /**
     *  The device supports immediate lock and unlock operations.
     */
    public final boolean TAPE_DRIVE_LOCK_UNLK_IMMED;

    /**
     *  The device moves the tape to a logical block address in a partition and returns as soon as the move begins.
     */
    public final boolean TAPE_DRIVE_LOG_BLK_IMMED;

    /**
     *  The device moves the tape to a logical block address in a partition.
     */
    public final boolean TAPE_DRIVE_LOGICAL_BLK;

    /**
     *  The device moves the tape forward (or backward) a specified number of blocks.
     */
    public final boolean TAPE_DRIVE_RELATIVE_BLKS;

    /**
     *  The device moves the tape backward over blocks, filemarks, or setmarks.
     */
    public final boolean TAPE_DRIVE_REVERSE_POSITION;

    /**
     *  The device supports immediate rewind operation.
     */
    public final boolean TAPE_DRIVE_REWIND_IMMEDIATE;

    /**
     *  The device moves the tape forward (or backward) to the first occurrence of a specified number of consecutive filemarks.
     */
    public final boolean TAPE_DRIVE_SEQUENTIAL_FMKS;

    /**
     *  The device moves the tape forward (or backward) to the first occurrence of a specified number of consecutive setmarks.
     */
    public final boolean TAPE_DRIVE_SEQUENTIAL_SMKS;

    /**
     *  The device supports setting the size of a fixed-length logical block or setting the variable-length block mode.
     */
    public final boolean TAPE_DRIVE_SET_BLOCK_SIZE;

    /**
     *  The device enables and disables hardware data compression.
     */
    public final boolean TAPE_DRIVE_SET_COMPRESSION;

    /**
     *  The device enables and disables hardware error correction.
     */
    public final boolean TAPE_DRIVE_SET_ECC;

    /**
     *  The device enables and disables data padding.
     */
    public final boolean TAPE_DRIVE_SET_PADDING;

    /**
     *  The device enables and disables the reporting of setmarks.
     */
    public final boolean TAPE_DRIVE_SET_REPORT_SMKS;

    /**
     *  The device moves the tape forward (or reverse) a specified number of setmarks.
     */
    public final boolean TAPE_DRIVE_SETMARKS;

    /**
     *  The device supports immediate spacing.
     */
    public final boolean TAPE_DRIVE_SPACE_IMMEDIATE;

    /**
     *  The device supports tape tensioning.
     */
    public final boolean TAPE_DRIVE_TENSION;

    /**
     *  The device supports immediate tape tensioning.
     */
    public final boolean TAPE_DRIVE_TENSION_IMMED;

    /**
     *  The device writes filemarks.
     */
    public final boolean TAPE_DRIVE_WRITE_FILEMARKS;

    /**
     *  The device writes long filemarks.
     */
    public final boolean TAPE_DRIVE_WRITE_LONG_FMKS;

    /**
     *  The device supports immediate writing of short and long filemarks.
     */
    public final boolean TAPE_DRIVE_WRITE_MARK_IMMED;

    /**
     *  The device writes setmarks.
     */
    public final boolean TAPE_DRIVE_WRITE_SETMARKS;

    /**
     *  The device writes short filemarks.
     */
    public final boolean TAPE_DRIVE_WRITE_SHORT_FMKS;

    /**
     * Capabilities passed to this object. Contains additional information, as this class only contains
     * the parsed high and low-bit fields
     */
    public final Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS RAW_CAPABILITIES;

    public ParsedTapeDriveCapabilities(Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS capabilities) {
        RAW_CAPABILITIES = capabilities;

        long highBits = capabilities.FeaturesHigh.longValue();
        long lowBits = capabilities.FeaturesLow.longValue();

        TAPE_DRIVE_COMPRESSION = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_COMPRESSION);
        TAPE_DRIVE_ECC = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_ECC);
        TAPE_DRIVE_REPORT_SMKS = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_REPORT_SMKS);
        TAPE_DRIVE_CLEAN_REQUESTS = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_CLEAN_REQUESTS);
        TAPE_DRIVE_EJECT_MEDIA = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_EJECT_MEDIA);
        TAPE_DRIVE_ERASE_BOP_ONLY = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_ERASE_BOP_ONLY);
        TAPE_DRIVE_ERASE_LONG = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_ERASE_LONG);
        TAPE_DRIVE_ERASE_IMMEDIATE = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_ERASE_IMMEDIATE);
        TAPE_DRIVE_ERASE_SHORT = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_ERASE_SHORT);
        TAPE_DRIVE_FIXED = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_FIXED);
        TAPE_DRIVE_FIXED_BLOCK = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_FIXED_BLOCK);
        TAPE_DRIVE_GET_ABSOLUTE_BLK = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_GET_ABSOLUTE_BLK);
        TAPE_DRIVE_GET_LOGICAL_BLK = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_GET_LOGICAL_BLK);
        TAPE_DRIVE_INITIATOR = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_INITIATOR);
        TAPE_DRIVE_PADDING = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_PADDING);
        TAPE_DRIVE_SELECT = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_SELECT);
        TAPE_DRIVE_SET_CMP_BOP_ONLY = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_SET_CMP_BOP_ONLY);
        TAPE_DRIVE_SET_EOT_WZ_SIZE = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_SET_EOT_WZ_SIZE);
        TAPE_DRIVE_TAPE_CAPACITY = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_TAPE_CAPACITY);
        TAPE_DRIVE_TAPE_REMAINING = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_TAPE_REMAINING);
        TAPE_DRIVE_VARIABLE_BLOCK = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_VARIABLE_BLOCK);
        TAPE_DRIVE_WRITE_PROTECT = checkWithBitmask(lowBits, Kernel32WithTape.TAPE_DRIVE_WRITE_PROTECT);

        TAPE_DRIVE_ABS_BLK_IMMED = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_ABS_BLK_IMMED);
        TAPE_DRIVE_ABSOLUTE_BLK = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_ABSOLUTE_BLK);
        TAPE_DRIVE_END_OF_DATA = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_END_OF_DATA);
        TAPE_DRIVE_FILEMARKS = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_FILEMARKS);
        TAPE_DRIVE_LOAD_UNLOAD = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_LOAD_UNLOAD);
        TAPE_DRIVE_LOAD_UNLD_IMMED = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_LOAD_UNLD_IMMED);
        TAPE_DRIVE_LOCK_UNLOCK = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_LOCK_UNLOCK);
        TAPE_DRIVE_LOCK_UNLK_IMMED = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_LOCK_UNLK_IMMED);
        TAPE_DRIVE_LOG_BLK_IMMED = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_LOG_BLK_IMMED);
        TAPE_DRIVE_LOGICAL_BLK = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_LOGICAL_BLK);
        TAPE_DRIVE_RELATIVE_BLKS = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_RELATIVE_BLKS);
        TAPE_DRIVE_REVERSE_POSITION = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_REVERSE_POSITION);
        TAPE_DRIVE_REWIND_IMMEDIATE = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_REWIND_IMMEDIATE);
        TAPE_DRIVE_SEQUENTIAL_FMKS = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_SEQUENTIAL_FMKS);
        TAPE_DRIVE_SEQUENTIAL_SMKS = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_SEQUENTIAL_SMKS);
        TAPE_DRIVE_SET_BLOCK_SIZE = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_SET_BLOCK_SIZE);
        TAPE_DRIVE_SET_COMPRESSION = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_SET_COMPRESSION);
        TAPE_DRIVE_SET_ECC = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_SET_ECC);
        TAPE_DRIVE_SET_PADDING = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_SET_PADDING);
        TAPE_DRIVE_SET_REPORT_SMKS = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_SET_REPORT_SMKS);
        TAPE_DRIVE_SETMARKS = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_SETMARKS);
        TAPE_DRIVE_SPACE_IMMEDIATE = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_SPACE_IMMEDIATE);
        TAPE_DRIVE_TENSION = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_TENSION);
        TAPE_DRIVE_TENSION_IMMED = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_TENSION_IMMED);
        TAPE_DRIVE_WRITE_FILEMARKS = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_WRITE_FILEMARKS);
        TAPE_DRIVE_WRITE_LONG_FMKS = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_WRITE_LONG_FMKS);
        TAPE_DRIVE_WRITE_MARK_IMMED = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_WRITE_MARK_IMMED);
        TAPE_DRIVE_WRITE_SETMARKS = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_WRITE_SETMARKS);
        TAPE_DRIVE_WRITE_SHORT_FMKS = checkWithBitmask(highBits, Kernel32WithTape.TAPE_DRIVE_WRITE_SHORT_FMKS);
    }

    /**
     * Checks if a given long contains a given bitmask
     * @param value Value to check
     * @param bitmask Bitmask to check the value against
     * @return
     */
    private static boolean checkWithBitmask(long value, long bitmask) {
        return (value & bitmask) == bitmask;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ParsedTapeDriveCapabilities.class.getSimpleName() + "[", "]")
                .add("TAPE_DRIVE_COMPRESSION=" + TAPE_DRIVE_COMPRESSION)
                .add("TAPE_DRIVE_CLEAN_REQUESTS=" + TAPE_DRIVE_CLEAN_REQUESTS)
                .add("TAPE_DRIVE_ECC=" + TAPE_DRIVE_ECC)
                .add("TAPE_DRIVE_EJECT_MEDIA=" + TAPE_DRIVE_EJECT_MEDIA)
                .add("TAPE_DRIVE_ERASE_BOP_ONLY=" + TAPE_DRIVE_ERASE_BOP_ONLY)
                .add("TAPE_DRIVE_ERASE_LONG=" + TAPE_DRIVE_ERASE_LONG)
                .add("TAPE_DRIVE_ERASE_IMMEDIATE=" + TAPE_DRIVE_ERASE_IMMEDIATE)
                .add("TAPE_DRIVE_ERASE_SHORT=" + TAPE_DRIVE_ERASE_SHORT)
                .add("TAPE_DRIVE_FIXED=" + TAPE_DRIVE_FIXED)
                .add("TAPE_DRIVE_FIXED_BLOCK=" + TAPE_DRIVE_FIXED_BLOCK)
                .add("TAPE_DRIVE_GET_ABSOLUTE_BLK=" + TAPE_DRIVE_GET_ABSOLUTE_BLK)
                .add("TAPE_DRIVE_GET_LOGICAL_BLK=" + TAPE_DRIVE_GET_LOGICAL_BLK)
                .add("TAPE_DRIVE_INITIATOR=" + TAPE_DRIVE_INITIATOR)
                .add("TAPE_DRIVE_PADDING=" + TAPE_DRIVE_PADDING)
                .add("TAPE_DRIVE_REPORT_SMKS=" + TAPE_DRIVE_REPORT_SMKS)
                .add("TAPE_DRIVE_SELECT=" + TAPE_DRIVE_SELECT)
                .add("TAPE_DRIVE_SET_CMP_BOP_ONLY=" + TAPE_DRIVE_SET_CMP_BOP_ONLY)
                .add("TAPE_DRIVE_SET_EOT_WZ_SIZE=" + TAPE_DRIVE_SET_EOT_WZ_SIZE)
                .add("TAPE_DRIVE_TAPE_CAPACITY=" + TAPE_DRIVE_TAPE_CAPACITY)
                .add("TAPE_DRIVE_TAPE_REMAINING=" + TAPE_DRIVE_TAPE_REMAINING)
                .add("TAPE_DRIVE_VARIABLE_BLOCK=" + TAPE_DRIVE_VARIABLE_BLOCK)
                .add("TAPE_DRIVE_WRITE_PROTECT=" + TAPE_DRIVE_WRITE_PROTECT)
                .add("TAPE_DRIVE_ABS_BLK_IMMED=" + TAPE_DRIVE_ABS_BLK_IMMED)
                .add("TAPE_DRIVE_ABSOLUTE_BLK=" + TAPE_DRIVE_ABSOLUTE_BLK)
                .add("TAPE_DRIVE_END_OF_DATA=" + TAPE_DRIVE_END_OF_DATA)
                .add("TAPE_DRIVE_FILEMARKS=" + TAPE_DRIVE_FILEMARKS)
                .add("TAPE_DRIVE_LOAD_UNLOAD=" + TAPE_DRIVE_LOAD_UNLOAD)
                .add("TAPE_DRIVE_LOAD_UNLD_IMMED=" + TAPE_DRIVE_LOAD_UNLD_IMMED)
                .add("TAPE_DRIVE_LOCK_UNLOCK=" + TAPE_DRIVE_LOCK_UNLOCK)
                .add("TAPE_DRIVE_LOCK_UNLK_IMMED=" + TAPE_DRIVE_LOCK_UNLK_IMMED)
                .add("TAPE_DRIVE_LOG_BLK_IMMED=" + TAPE_DRIVE_LOG_BLK_IMMED)
                .add("TAPE_DRIVE_LOGICAL_BLK=" + TAPE_DRIVE_LOGICAL_BLK)
                .add("TAPE_DRIVE_RELATIVE_BLKS=" + TAPE_DRIVE_RELATIVE_BLKS)
                .add("TAPE_DRIVE_REVERSE_POSITION=" + TAPE_DRIVE_REVERSE_POSITION)
                .add("TAPE_DRIVE_REWIND_IMMEDIATE=" + TAPE_DRIVE_REWIND_IMMEDIATE)
                .add("TAPE_DRIVE_SEQUENTIAL_FMKS=" + TAPE_DRIVE_SEQUENTIAL_FMKS)
                .add("TAPE_DRIVE_SEQUENTIAL_SMKS=" + TAPE_DRIVE_SEQUENTIAL_SMKS)
                .add("TAPE_DRIVE_SET_BLOCK_SIZE=" + TAPE_DRIVE_SET_BLOCK_SIZE)
                .add("TAPE_DRIVE_SET_COMPRESSION=" + TAPE_DRIVE_SET_COMPRESSION)
                .add("TAPE_DRIVE_SET_ECC=" + TAPE_DRIVE_SET_ECC)
                .add("TAPE_DRIVE_SET_PADDING=" + TAPE_DRIVE_SET_PADDING)
                .add("TAPE_DRIVE_SET_REPORT_SMKS=" + TAPE_DRIVE_SET_REPORT_SMKS)
                .add("TAPE_DRIVE_SETMARKS=" + TAPE_DRIVE_SETMARKS)
                .add("TAPE_DRIVE_SPACE_IMMEDIATE=" + TAPE_DRIVE_SPACE_IMMEDIATE)
                .add("TAPE_DRIVE_TENSION=" + TAPE_DRIVE_TENSION)
                .add("TAPE_DRIVE_TENSION_IMMED=" + TAPE_DRIVE_TENSION_IMMED)
                .add("TAPE_DRIVE_WRITE_FILEMARKS=" + TAPE_DRIVE_WRITE_FILEMARKS)
                .add("TAPE_DRIVE_WRITE_LONG_FMKS=" + TAPE_DRIVE_WRITE_LONG_FMKS)
                .add("TAPE_DRIVE_WRITE_MARK_IMMED=" + TAPE_DRIVE_WRITE_MARK_IMMED)
                .add("TAPE_DRIVE_WRITE_SETMARKS=" + TAPE_DRIVE_WRITE_SETMARKS)
                .add("TAPE_DRIVE_WRITE_SHORT_FMKS=" + TAPE_DRIVE_WRITE_SHORT_FMKS)
                .add("RAW_CAPABILITIES=" + RAW_CAPABILITIES)
                .toString();
    }
}
