package de.notepass.notaping.core.access.jTapeAccess.impl.windows;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.win32.W32APIOptions;

import java.util.Arrays;
import java.util.List;

public interface Kernel32WithTape extends Kernel32 {
    Kernel32WithTape INSTANCE = Native.load("kernel32", Kernel32WithTape.class, W32APIOptions.DEFAULT_OPTIONS);

    /**
     *  The device supports hardware data compression.
     */
    public final static int TAPE_DRIVE_COMPRESSION = 0x00020000;

    /**
     *  The device can report if cleaning is required.
     */
    public final static int TAPE_DRIVE_CLEAN_REQUESTS = 0x02000000;

    /**
     *  The device supports hardware error correction.
     */
    public final static int TAPE_DRIVE_ECC = 0x00010000;

    /**
     *  The device physically ejects the tape on a software eject.
     */
    public final static int TAPE_DRIVE_EJECT_MEDIA = 0x01000000;

    /**
     *  The device performs the erase operation from the beginning-of-partition marker only.
     */
    public final static int TAPE_DRIVE_ERASE_BOP_ONLY = 0x00000040;

    /**
     *  The device performs a long erase operation.
     */
    public final static int TAPE_DRIVE_ERASE_LONG = 0x00000020;

    /**
     *  The device performs an immediate erase operation - that is, it returns when the erase operation begins.
     */
    public final static int TAPE_DRIVE_ERASE_IMMEDIATE = 0x00000080;

    /**
     *  The device performs a short erase operation.
     */
    public final static int TAPE_DRIVE_ERASE_SHORT = 0x00000010;

    /**
     *  The device creates fixed data partitions.
     */
    public final static int TAPE_DRIVE_FIXED = 0x00000001;

    /**
     *  The device supports fixed-length block mode.
     */
    public final static int TAPE_DRIVE_FIXED_BLOCK = 0x00000400;

    /**
     *  The device provides the current device-specific block address.
     */
    public final static int TAPE_DRIVE_GET_ABSOLUTE_BLK = 0x00100000;

    /**
     *  The device provides the current logical block address (and logical tape partition).
     */
    public final static int TAPE_DRIVE_GET_LOGICAL_BLK = 0x00200000;

    /**
     *  The device creates initiator-defined partitions.
     */
    public final static int TAPE_DRIVE_INITIATOR = 0x00000004;

    /**
     *  The device supports data padding.
     */
    public final static int TAPE_DRIVE_PADDING = 0x00040000;

    /**
     *  The device supports setmark reporting.
     */
    public final static int TAPE_DRIVE_REPORT_SMKS = 0x00080000;

    /**
     *  The device creates select data partitions.
     */
    public final static int TAPE_DRIVE_SELECT = 0x00000002;

    /**
     *  The device must be at the beginning of a partition before it can set compression on.
     */
    public final static int TAPE_DRIVE_SET_CMP_BOP_ONLY = 0x04000000;

    /**
     *  The device supports setting the end-of-medium warning size.
     */
    public final static int TAPE_DRIVE_SET_EOT_WZ_SIZE = 0x00400000;

    /**
     *  The device returns the maximum capacity of the tape.
     */
    public final static int TAPE_DRIVE_TAPE_CAPACITY = 0x00000100;

    /**
     *  The device returns the remaining capacity of the tape.
     */
    public final static int TAPE_DRIVE_TAPE_REMAINING = 0x00000200;

    /**
     *  The device supports variable-length block mode.
     */
    public final static int TAPE_DRIVE_VARIABLE_BLOCK = 0x00000800;

    /**
     *  The device returns an error if the tape is write-enabled or write-protected.
     */
    public final static int TAPE_DRIVE_WRITE_PROTECT = 0x00001000;

    /**
     *  The device moves the tape to a device-specific block address and returns as soon as the move begins.
     */
    public final static int TAPE_DRIVE_ABS_BLK_IMMED = 0x80002000;

    /**
     *  The device moves the tape to a device specific block address.
     */
    public final static int TAPE_DRIVE_ABSOLUTE_BLK = 0x80001000;

    /**
     *  The device moves the tape to the end-of-data marker in a partition.
     */
    public final static int TAPE_DRIVE_END_OF_DATA = 0x80010000;

    /**
     *  The device moves the tape forward (or backward) a specified number of filemarks.
     */
    public final static int TAPE_DRIVE_FILEMARKS = 0x80040000;

    /**
     *  The device enables and disables the device for further operations.
     */
    public final static int TAPE_DRIVE_LOAD_UNLOAD = 0x80000001;

    /**
     *  The device supports immediate load and unload operations.
     */
    public final static int TAPE_DRIVE_LOAD_UNLD_IMMED = 0x80000020;

    /**
     *  The device enables and disables the tape ejection mechanism.
     */
    public final static int TAPE_DRIVE_LOCK_UNLOCK = 0x80000004;

    /**
     *  The device supports immediate lock and unlock operations.
     */
    public final static int TAPE_DRIVE_LOCK_UNLK_IMMED = 0x80000080;

    /**
     *  The device moves the tape to a logical block address in a partition and returns as soon as the move begins.
     */
    public final static int TAPE_DRIVE_LOG_BLK_IMMED = 0x80008000;

    /**
     *  The device moves the tape to a logical block address in a partition.
     */
    public final static int TAPE_DRIVE_LOGICAL_BLK = 0x80004000;

    /**
     *  The device moves the tape forward (or backward) a specified number of blocks.
     */
    public final static int TAPE_DRIVE_RELATIVE_BLKS = 0x80020000;

    /**
     *  The device moves the tape backward over blocks, filemarks, or setmarks.
     */
    public final static int TAPE_DRIVE_REVERSE_POSITION = 0x80400000;

    /**
     *  The device supports immediate rewind operation.
     */
    public final static int TAPE_DRIVE_REWIND_IMMEDIATE = 0x80000008;

    /**
     *  The device moves the tape forward (or backward) to the first occurrence of a specified number of consecutive filemarks.
     */
    public final static int TAPE_DRIVE_SEQUENTIAL_FMKS = 0x80080000;

    /**
     *  The device moves the tape forward (or backward) to the first occurrence of a specified number of consecutive setmarks.
     */
    public final static int TAPE_DRIVE_SEQUENTIAL_SMKS = 0x80200000;

    /**
     *  The device supports setting the size of a fixed-length logical block or setting the variable-length block mode.
     */
    public final static int TAPE_DRIVE_SET_BLOCK_SIZE = 0x80000010;

    /**
     *  The device enables and disables hardware data compression.
     */
    public final static int TAPE_DRIVE_SET_COMPRESSION = 0x80000200;

    /**
     *  The device enables and disables hardware error correction.
     */
    public final static int TAPE_DRIVE_SET_ECC = 0x80000100;

    /**
     *  The device enables and disables data padding.
     */
    public final static int TAPE_DRIVE_SET_PADDING = 0x80000400;

    /**
     *  The device enables and disables the reporting of setmarks.
     */
    public final static int TAPE_DRIVE_SET_REPORT_SMKS = 0x80000800;

    /**
     *  The device moves the tape forward (or reverse) a specified number of setmarks.
     */
    public final static int TAPE_DRIVE_SETMARKS = 0x80100000;

    /**
     *  The device supports immediate spacing.
     */
    public final static int TAPE_DRIVE_SPACE_IMMEDIATE = 0x80800000;

    /**
     *  The device supports tape tensioning.
     */
    public final static int TAPE_DRIVE_TENSION = 0x80000002;

    /**
     *  The device supports immediate tape tensioning.
     */
    public final static int TAPE_DRIVE_TENSION_IMMED = 0x80000040;

    /**
     *  The device writes filemarks.
     */
    public final static int TAPE_DRIVE_WRITE_FILEMARKS = 0x82000000;

    /**
     *  The device writes long filemarks.
     */
    public final static int TAPE_DRIVE_WRITE_LONG_FMKS = 0x88000000;

    /**
     *  The device supports immediate writing of short and long filemarks.
     */
    public final static int TAPE_DRIVE_WRITE_MARK_IMMED = 0x90000000;

    /**
     *  The device writes setmarks.
     */
    public final static int TAPE_DRIVE_WRITE_SETMARKS = 0x81000000;

    /**
     *  The device writes short filemarks.
     */
    public final static int TAPE_DRIVE_WRITE_SHORT_FMKS = 0x84000000;

    /* ==========================================Prepare tape========================================= */

    /**
     *  Performs a low-level format of the tape. Currently, only the QIC117 device supports this feature.
     */
    public final static int TAPE_FORMAT = 5;

    /**
     *  Loads the tape and moves the tape to the beginning.
     */
    public final static int TAPE_LOAD = 0;

    /**
     *  Locks the tape ejection mechanism so that the tape is not ejected accidentally.
     */
    public final static int TAPE_LOCK = 3;

    /**
     *  Adjusts the tension by moving the tape to the end of the tape and back to the beginning. This option is not supported by all devices. This value is ignored if it is not supported.
     */
    public final static int TAPE_TENSION = 2;

    /**
     * Moves the tape to the beginning for removal from the device. After a successful unload operation, the device returns errors to applications that attempt to access the tape, until the tape is loaded again.
     */
    public final static int TAPE_UNLOAD = 1;

    /**
     *  Unlocks the tape ejection mechanism.
     */
    public final static int TAPE_UNLOCK = 4;


    /* ==================================Set Tape Position========================== */

    /**
     *  Moves the tape to the device-specific block address specified by the dwOffsetLow and dwOffsetHigh parameters. The dwPartition parameter is ignored.
     */
    public final static int TAPE_ABSOLUTE_BLOCK = 1;

    /**
     * Moves the tape to the block address specified by dwOffsetLow and dwOffsetHigh in the partition specified by dwPartition.
     */
    public final static int TAPE_LOGICAL_BLOCK = 2;

    /**
     * Moves the tape to the beginning of the current partition. The dwPartition, dwOffsetLow, and dwOffsetHigh parameters are ignored.
     */
    public final static int TAPE_REWIND = 0;

    /**
     *  Moves the tape to the end of the data on the partition specified by dwPartition.
     */
    public final static int TAPE_SPACE_END_OF_DATA = 4;

    /**
     * Moves the tape forward (or backward) the number of filemarks specified by dwOffsetLow and dwOffsetHigh in the current partition. The dwPartition parameter is ignored.
     */
    public final static int TAPE_SPACE_FILEMARKS = 6;

    /**
     *  Moves the tape forward (or backward) the number of blocks specified by dwOffsetLow and dwOffsetHigh in the current partition. The dwPartition parameter is ignored.
     */
    public final static int TAPE_SPACE_RELATIVE_BLOCKS = 5;

    /**
     * Moves the tape forward (or backward) to the first occurrence of n filemarks in the current partition, where n is the number specified by dwOffsetLow and dwOffsetHigh. The dwPartition parameter is ignored.
     */
    public final static int TAPE_SPACE_SEQUENTIAL_FMKS = 7;

    /**
     *  Moves the tape forward (or backward) to the first occurrence of n setmarks in the current partition, where n is the number specified by dwOffsetLow and dwOffsetHigh. The dwPartition parameter is ignored.
     */
    public final static int TAPE_SPACE_SEQUENTIAL_SMKS = 9;

    /**
     *  Moves the tape forward (or backward) the number of setmarks specified by dwOffsetLow and dwOffsetHigh in the current partition. The dwPartition parameter is ignored.
     */
    public final static int TAPE_SPACE_SETMARKS = 8;

    /* ============== Tape metadata interactions =================*/

    /**
     *  Retrieves information about the tape device.
     */
    public static final int GET_TAPE_DRIVE_INFORMATION = 1;

    /**
     *  Retrieves information about the tape in the tape device.
     */
    public static final int GET_TAPE_MEDIA_INFORMATION = 0;

    /**
     * Partitions the tape based on the device's default definition of partitions. The dwCount and dwSize parameters are ignored.
     */
    public static final int TAPE_FIXED_PARTITIONS = 0;

    /**
     * Partitions the tape into the number of partitions specified by dwCount. The dwSize parameter is ignored. The size of the partitions is determined by the device's default partition size. For more specific information, see the documentation for your tape device.
     */
    public static final int TAPE_SELECT_PARTITIONS = 1;

    /**
     * Partitions the tape into the number and size of partitions specified by dwCount and dwSize, respectively, except for the last partition. The size of the last partition is the remainder of the tape.
     */
    public static final int TAPE_INITIATOR_PARTITIONS = 2;

    /* =========== Setting tape Position ============= */
    /**
     * The lpdwOffsetLow and lpdwOffsetHigh parameters receive the device-specific block address. The dwPartition parameter receives zero.
     */
    public static final int TAPE_ABSOLUTE_POSITION = 0;

    /**
     * The lpdwOffsetLow and lpdwOffsetHigh parameters receive the logical block address. The dwPartition parameter receives the logical tape partition.
     */
    public static final int TAPE_LOGICAL_POSITION = 0;

    /**
     *  Sets the device-specific information specified by lpTapeInformation.
     */
    public static final int SET_TAPE_DRIVE_INFORMATION = 1;

    /**
     *  Sets the tape-specific information specified by the lpTapeInformation parameter.
     */
    public static final int SET_TAPE_MEDIA_INFORMATION = 0;

    /* =============== Filemark type ============ */
    /**
     *  Writes the number of filemarks specified by the dwTapemarkCount parameter.
     */
    public static final int TAPE_FILEMARKS = 1;

    /**
     *  Writes the number of long filemarks specified by dwTapemarkCount.
     */
    public static final int TAPE_LONG_FILEMARKS = 3;

    /**
     *  Writes the number of setmarks specified by dwTapemarkCount.
     */
    public static final int TAPE_SETMARKS = 0;

    /**
     *  Writes the number of short filemarks specified by dwTapemarkCount.
     */
    public static final int TAPE_SHORT_FILEMARKS = 2;


    public static class TAPE_SET_DRIVE_PARAMETERS extends Structure {
        public TAPE_SET_DRIVE_PARAMETERS() {
            super();
        }

        public TAPE_SET_DRIVE_PARAMETERS(Pointer peer) {
            super(peer);
        }

        public static class ByReference extends TAPE_SET_DRIVE_PARAMETERS implements Structure.ByReference {

        };

        public static class ByValue extends TAPE_SET_DRIVE_PARAMETERS implements Structure.ByValue {

        };

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList(
                    "ECC",
                    "Compression",
                    "DataPadding",
                    "ReportSetmarks",
                    "EOTWarningZoneSize"
            );
        }

        /**
         * If this member is TRUE, hardware error correction is supported. Otherwise, it is not.
         */
        public BYTE ECC;

        /**
         * If this member is TRUE, hardware data compression is enabled. Otherwise, it is disabled.
         */
        public BYTE Compression;

        /**
         * If this member is TRUE, data padding is enabled. Otherwise, it is disabled. Data padding keeps the tape streaming at a constant speed.
         */
        public BYTE DataPadding;

        /**
         * If this member is TRUE, setmark reporting is enabled. Otherwise, it is disabled.
         */
        public BYTE ReportSetmarks;

        /**
         * Number of bytes between the end-of-tape warning and the physical end of the tape.
         */
        public DWORD EOTWarningZoneSize;
    }

    public static class TAPE_SET_MEDIA_PARAMETERS extends Structure {
        public TAPE_SET_MEDIA_PARAMETERS() {
            super();
        }

        public TAPE_SET_MEDIA_PARAMETERS(Pointer peer) {
            super(peer);
        }

        public static class ByReference extends TAPE_SET_MEDIA_PARAMETERS implements Structure.ByReference {

        };

        public static class ByValue extends TAPE_SET_MEDIA_PARAMETERS implements Structure.ByValue {

        };

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList(
                    "BlockSize"
            );
        }

        /**
         * Number of bytes per block. Maximum and minimum block sizes can be obtained by calling the GetTapeParameters function.
         */
        public DWORD BlockSize;
    }

    public class TAPE_GET_MEDIA_PARAMETERS extends Structure {
        public TAPE_GET_MEDIA_PARAMETERS() {
            super();
        }

        public TAPE_GET_MEDIA_PARAMETERS(Pointer peer) {
            super(peer);
        }

        public static class ByReference extends TAPE_GET_MEDIA_PARAMETERS implements Structure.ByReference {

        };

        public static class ByValue extends TAPE_GET_MEDIA_PARAMETERS implements Structure.ByValue {

        };

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList(
                    "Capacity",
                    "Remaining",
                    "BlockSize",
                    "PartitionCount",
                    "WriteProtected"
            );
        }

        /**
         * Total number of bytes on the current tape partition.
         */
        public LARGE_INTEGER Capacity;

        /**
         * Number of bytes between the current position and the end of the current tape partition.
         */
        public LARGE_INTEGER Remaining;

        /**
         * Number of bytes per block.
         */
        public DWORD BlockSize;

        /**
         * Number of partitions on the tape.
         */
        public DWORD PartitionCount;

        /**
         * If this member is TRUE, the tape is write-protected. Otherwise, it is not.
         */
        public BOOL WriteProtected;
    }

    public class TAPE_GET_DRIVE_PARAMETERS extends Structure {
        public TAPE_GET_DRIVE_PARAMETERS() {
            super();
        }

        public TAPE_GET_DRIVE_PARAMETERS(Pointer peer) {
            super(peer);
        }

        public static class ByReference extends TAPE_GET_DRIVE_PARAMETERS implements Structure.ByReference {

        };

        public static class ByValue extends TAPE_GET_DRIVE_PARAMETERS implements Structure.ByValue {

        };

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList(
                    "ECC",
                    "Compression",
                    "DataPadding",
                    "ReportSetmarks",
                    "DefaultBlockSize",
                    "MaximumBlockSize",
                    "MinimumBlockSize",
                    "MaximumPartitionCount",
                    "FeaturesLow",
                    "FeaturesHigh",
                    "EOTWarningZoneSize"
            );
        }

        /**
         * If this member is TRUE, the device supports hardware error correction. Otherwise, it does not.
         */
        public BYTE ECC;

        /**
         * If this member is TRUE, hardware data compression is enabled. Otherwise, it is disabled.
         */
        public BYTE Compression;

        /**
         * If this member is TRUE, data padding is enabled. Otherwise, it is disabled. Data padding keeps the tape streaming at a constant speed.
         */
        public BYTE DataPadding;

        /**
         * If this member is TRUE, setmark reporting is enabled. Otherwise, it is disabled.
         */
        public BYTE ReportSetmarks;

        /**
         * Device's default fixed block size, in bytes.
         */
        public DWORD DefaultBlockSize;

        /**
         * Device's maximum block size, in bytes.
         */
        public DWORD MaximumBlockSize;

        /**
         * Device's minimum block size, in bytes.
         */
        public DWORD MinimumBlockSize;

        /**
         * Maximum number of partitions that can be created on the device.
         */
        public DWORD MaximumPartitionCount;

        /**
         *Low-order bits of the device features flag. This member can be one or more of following values.<br/>
         * <ul>
         * <li>TAPE_DRIVE_COMPRESSION (0x00020000): The device supports hardware data compression.</li>
         * <li>TAPE_DRIVE_CLEAN_REQUESTS (0x02000000): The device can report if cleaning is required.</li>
         * <li>TAPE_DRIVE_ECC (0x00010000): The device supports hardware error correction.</li>
         * <li>TAPE_DRIVE_EJECT_MEDIA (0x01000000): The device physically ejects the tape on a software eject.</li>
         * <li>TAPE_DRIVE_ERASE_BOP_ONLY (0x00000040): The device performs the erase operation from the beginning-of-partition marker only.</li>
         * <li>TAPE_DRIVE_ERASE_LONG (0x00000020): The device performs a long erase operation.</li>
         * <li>TAPE_DRIVE_ERASE_IMMEDIATE (0x00000080): The device performs an immediate erase operation — that is, it returns when the erase operation begins.</li>
         * <li>TAPE_DRIVE_ERASE_SHORT (0x00000010): The device performs a short erase operation.</li>
         * <li>TAPE_DRIVE_FIXED (0x00000001): The device creates fixed data partitions.</li>
         * <li>TAPE_DRIVE_FIXED_BLOCK (0x00000400): The device supports fixed-length block mode.</li>
         * <li>TAPE_DRIVE_GET_ABSOLUTE_BLK (0x00100000): The device provides the current device-specific block address.</li>
         * <li>TAPE_DRIVE_GET_LOGICAL_BLK (0x00200000): The device provides the current logical block address (and logical tape partition).</li>
         * <li>TAPE_DRIVE_INITIATOR (0x00000004): The device creates initiator-defined partitions.</li>
         * <li>TAPE_DRIVE_PADDING (0x00040000): The device supports data padding.</li>
         * <li>TAPE_DRIVE_REPORT_SMKS (0x00080000): The device supports setmark reporting.</li>
         * <li>TAPE_DRIVE_SELECT (0x00000002): The device creates select data partitions.</li>
         * <li>TAPE_DRIVE_SET_CMP_BOP_ONLY (0x04000000): The device must be at the beginning of a partition before it can set compression on.</li>
         * <li>TAPE_DRIVE_SET_EOT_WZ_SIZE (0x00400000): The device supports setting the end-of-medium warning size.</li>
         * <li>TAPE_DRIVE_TAPE_CAPACITY (0x00000100): The device returns the maximum capacity of the tape.</li>
         * <li>TAPE_DRIVE_TAPE_REMAINING (0x00000200): The device returns the remaining capacity of the tape.</li>
         * <li>TAPE_DRIVE_VARIABLE_BLOCK (0x00000800): The device supports variable-length block mode.</li>
         * <li>TAPE_DRIVE_WRITE_PROTECT (0x00001000): The device returns an error if the tape is write-enabled or write-protected. </li>
         * </ul>
         */
        public DWORD FeaturesLow;

        /**
         *High-order bits of the device features flag. This member can be one or more of the following values.<br/>
         * <ul>
         * <li>TAPE_DRIVE_ABS_BLK_IMMED (0x80002000): The device moves the tape to a device-specific block address and returns as soon as the move begins.</li>
         * <li>TAPE_DRIVE_ABSOLUTE_BLK (0x80001000): The device moves the tape to a device specific block address.</li>
         * <li>TAPE_DRIVE_END_OF_DATA (0x80010000): The device moves the tape to the end-of-data marker in a partition.</li>
         * <li>TAPE_DRIVE_FILEMARKS (0x80040000): The device moves the tape forward (or backward) a specified number of filemarks.</li>
         * <li>TAPE_DRIVE_LOAD_UNLOAD (0x80000001): The device enables and disables the device for further operations.</li>
         * <li>TAPE_DRIVE_LOAD_UNLD_IMMED (0x80000020): The device supports immediate load and unload operations.</li>
         * <li>TAPE_DRIVE_LOCK_UNLOCK (0x80000004): The device enables and disables the tape ejection mechanism.</li>
         * <li>TAPE_DRIVE_LOCK_UNLK_IMMED (0x80000080): The device supports immediate lock and unlock operations.</li>
         * <li>TAPE_DRIVE_LOG_BLK_IMMED (0x80008000): The device moves the tape to a logical block address in a partition and returns as soon as the move begins.</li>
         * <li>TAPE_DRIVE_LOGICAL_BLK (0x80004000): The device moves the tape to a logical block address in a partition.</li>
         * <li>TAPE_DRIVE_RELATIVE_BLKS (0x80020000): The device moves the tape forward (or backward) a specified number of blocks.</li>
         * <li>TAPE_DRIVE_REVERSE_POSITION (0x80400000): The device moves the tape backward over blocks, filemarks, or setmarks.</li>
         * <li>TAPE_DRIVE_REWIND_IMMEDIATE (0x80000008): The device supports immediate rewind operation.</li>
         * <li>TAPE_DRIVE_SEQUENTIAL_FMKS (0x80080000): The device moves the tape forward (or backward) to the first occurrence of a specified number of consecutive filemarks.</li>
         * <li>TAPE_DRIVE_SEQUENTIAL_SMKS (0x80200000): The device moves the tape forward (or backward) to the first occurrence of a specified number of consecutive setmarks.</li>
         * <li>TAPE_DRIVE_SET_BLOCK_SIZE (0x80000010): The device supports setting the size of a fixed-length logical block or setting the variable-length block mode.</li>
         * <li>TAPE_DRIVE_SET_COMPRESSION (0x80000200): The device enables and disables hardware data compression.</li>
         * <li>TAPE_DRIVE_SET_ECC (0x80000100): The device enables and disables hardware error correction.</li>
         * <li>TAPE_DRIVE_SET_PADDING (0x80000400): The device enables and disables data padding.</li>
         * <li>TAPE_DRIVE_SET_REPORT_SMKS (0x80000800): The device enables and disables the reporting of setmarks.</li>
         * <li>TAPE_DRIVE_SETMARKS (0x80100000): The device moves the tape forward (or reverse) a specified number of setmarks.</li>
         * <li>TAPE_DRIVE_SPACE_IMMEDIATE (0x80800000): The device supports immediate spacing.</li>
         * <li>TAPE_DRIVE_TENSION (0x80000002): The device supports tape tensioning.</li>
         * <li>TAPE_DRIVE_TENSION_IMMED (0x80000040): The device supports immediate tape tensioning.</li>
         * <li>TAPE_DRIVE_WRITE_FILEMARKS (0x82000000): The device writes filemarks.</li>
         * <li>TAPE_DRIVE_WRITE_LONG_FMKS (0x88000000): The device writes long filemarks.</li>
         * <li>TAPE_DRIVE_WRITE_MARK_IMMED (0x90000000): The device supports immediate writing of short and long filemarks.</li>
         * <li>TAPE_DRIVE_WRITE_SETMARKS (0x81000000): The device writes setmarks.</li>
         * <li>TAPE_DRIVE_WRITE_SHORT_FMKS (0x84000000): The device writes short filemarks. </li>
         * </ul>
         */
        public DWORD FeaturesHigh;

        /**
         * Indicates the number of bytes between the end-of-tape warning and the physical end of the tape.
         */
        public DWORD EOTWarningZoneSize;
    }

    /**
     * The PrepareTape function prepares the tape to be accessed or removed.
     * @param hDevice Handle to the device preparing the tape. This handle is created by using the {@link #CreateFile(String, int, int, SECURITY_ATTRIBUTES, int, int, HANDLE)} function.
     * @param dwOperation Tape device preparation. This parameter can be one of the following values.<br/>
     *                    <ul>
     *                    <li>TAPE_FORMAT (5L): Performs a low-level format of the tape. Currently, only the QIC117 device supports this feature.</li>
     *                    <li>TAPE_LOAD (0L): Loads the tape and moves the tape to the beginning.</li>
     *                    <li>TAPE_LOCK (3L): Locks the tape ejection mechanism so that the tape is not ejected accidentally.</li>
     *                    <li>TAPE_TENSION (2L): Adjusts the tension by moving the tape to the end of the tape and back to the beginning. This option is not supported by all devices. This value is ignored if it is not supported.</li>
     *                    <li>TAPE_UNLOAD (1L): Moves the tape to the beginning for removal from the device. After a successful unload operation, the device returns errors to applications that attempt to access the tape, until the tape is loaded again.</li>
     *                    <li>TAPE_UNLOCK (4L): Unlocks the tape ejection mechanism.</li>
     *                    </ul>
     * @param bImmediate If this parameter is TRUE, the function returns immediately. If it is FALSE, the function does not return until the operation has been completed.
     * @return If the function succeeds, the return value is NO_ERROR.<br>
     * If the function fails, it can return one of the following error codes.
     *
     * <ul>
     * <li>ERROR_BEGINNING_OF_MEDIA (1102L): An attempt to access data before the beginning-of-medium marker failed.</li>
     * <li>ERROR_BUS_RESET (1111L): A reset condition was detected on the bus.</li>
     * <li>ERROR_DEVICE_NOT_PARTITIONED (1107L): The partition information could not be found when a tape was being loaded.</li>
     * <li>ERROR_END_OF_MEDIA (1100L): The end-of-tape marker was reached during an operation.</li>
     * <li>ERROR_FILEMARK_DETECTED (1101L): A filemark was reached during an operation.</li>
     * <li>ERROR_INVALID_BLOCK_LENGTH (1106L): The block size is incorrect on a new tape in a multivolume partition.</li>
     * <li>ERROR_MEDIA_CHANGED (1110L): The tape that was in the drive has been replaced or removed.</li>
     * <li>ERROR_NO_DATA_DETECTED (1104L): The end-of-data marker was reached during an operation.</li>
     * <li>ERROR_NO_MEDIA_IN_DRIVE (1112L): There is no media in the drive.</li>
     * <li>ERROR_NOT_SUPPORTED (50L): The tape driver does not support a requested function.</li>
     * <li>ERROR_PARTITION_FAILURE (1105L): The tape could not be partitioned.</li>
     * <li>ERROR_SETMARK_DETECTED (1103L): A setmark was reached during an operation.</li>
     * <li>ERROR_UNABLE_TO_LOCK_MEDIA (1108L): An attempt to lock the ejection mechanism failed.</li>
     * <li>ERROR_UNABLE_TO_UNLOAD_MEDIA (1109L): An attempt to unload the tape failed.</li>
     * <li>ERROR_WRITE_PROTECT (19L): The media is write protected. </li>
     * </ul>
     */
    public DWORD PrepareTape(HANDLE hDevice, DWORD dwOperation, BOOL bImmediate);

    public DWORD SetTapePosition(HANDLE hDevice, DWORD dwPositionMethod, DWORD dwPartition, DWORD dwOffsetLow, DWORD dwOffsetHigh, BOOL bImmediate);

    /**
     * The GetTapePosition function retrieves the current address of the tape, in logical or absolute blocks.
     *
     * @param hDevice        Handle to the device on which to get the tape position. This handle is created by using {@link #CreateFile(String, int, int, SECURITY_ATTRIBUTES, int, int, HANDLE)}.
     * @param dwPositionType Type of address to obtain. This parameter can be one of the following values.<br/>
     *                       <ul>
     *                          <li>TAPE_ABSOLUTE_POSITION (0L): The lpdwOffsetLow and lpdwOffsetHigh parameters receive the device-specific block address. The dwPartition parameter receives zero.</li>
     *                          <li>TAPE_LOGICAL_POSITION (1L): The lpdwOffsetLow and lpdwOffsetHigh parameters receive the logical block address. The dwPartition parameter receives the logical tape partition.</li>
     *                       </ul>
     * @param lpdwPartition  Pointer to a variable that receives the number of the current tape partition. Partitions are numbered logically from 1 through n, where 1 is the first partition on the tape and n is the last. When a device-specific block address is retrieved, or if the device supports only one partition, this parameter receives zero.
     * @param lpdwOffsetLow  Pointer to a variable that receives the low-order bits of the current tape position.
     * @param lpdwOffsetHigh Pointer to a variable that receives the high-order bits of the current tape position. This parameter can be NULL if the high-order bits are not required.
     * @return If the function succeeds, the return value is NO_ERROR.<br/>
     * If the function fails, it can return one of the following error codes.<br/>
     * <ul>
     *     <li>ERROR_BEGINNING_OF_MEDIA (1102L): An attempt to access data before the beginning-of-medium marker failed.</li>
     * <li>ERROR_BUS_RESET (1111L): A reset condition was detected on the bus.</li>
     * <li>ERROR_DEVICE_NOT_PARTITIONED (1107L): The partition information could not be found when a tape was being loaded.</li>
     * <li>ERROR_END_OF_MEDIA (1100L): The end-of-tape marker was reached during an operation.</li>
     * <li>ERROR_FILEMARK_DETECTED (1101L): A filemark was reached during an operation.</li>
     * <li>ERROR_INVALID_BLOCK_LENGTH (1106L): The block size is incorrect on a new tape in a multivolume partition.</li>
     * <li>ERROR_MEDIA_CHANGED (1110L): The tape that was in the drive has been replaced or removed.</li>
     * <li>ERROR_NO_DATA_DETECTED (1104L): The end-of-data marker was reached during an operation.</li>
     * <li>ERROR_NO_MEDIA_IN_DRIVE (1112L): There is no media in the drive.</li>
     * <li>ERROR_NOT_SUPPORTED (50L): The tape driver does not support a requested function.</li>
     * <li>ERROR_PARTITION_FAILURE (1105L): The tape could not be partitioned.</li>
     * <li>ERROR_SETMARK_DETECTED (1103L): A setmark was reached during an operation.</li>
     * <li>ERROR_UNABLE_TO_LOCK_MEDIA (1108L): An attempt to lock the ejection mechanism failed.</li>
     * <li>ERROR_UNABLE_TO_UNLOAD_MEDIA (1109L): An attempt to unload the tape failed.</li>
     * <li>ERROR_WRITE_PROTECT (19L): The media is write protected. </li>
     * </ul>
     */
    public DWORD GetTapePosition(HANDLE hDevice, DWORD dwPositionType, DWORDByReference lpdwPartition, DWORDByReference lpdwOffsetLow, DWORDByReference lpdwOffsetHigh);

    /**
     * The SetTapePosition function sets the tape position on the specified device.
     * @param hDevice Handle to the device on which to set the tape position. This handle is created by using the CreateFile function.
     * @param dwPositionMethod Type of positioning to perform. This parameter must be one of the following values.<br/>
     *                         <ul>
     *                         <li>TAPE_ABSOLUTE_BLOCK (1L): Moves the tape to the device-specific block address specified by the dwOffsetLow and dwOffsetHigh parameters. The dwPartition parameter is ignored.</li>
     * <li>TAPE_LOGICAL_BLOCK (2L): Moves the tape to the block address specified by dwOffsetLow and dwOffsetHigh in the partition specified by dwPartition.</li>
     * <li>TAPE_REWIND (0L): Moves the tape to the beginning of the current partition. The dwPartition, dwOffsetLow, and dwOffsetHigh parameters are ignored.</li>
     * <li>TAPE_SPACE_END_OF_DATA (4L): Moves the tape to the end of the data on the partition specified by dwPartition.</li>
     * <li>TAPE_SPACE_FILEMARKS (6L): Moves the tape forward (or backward) the number of filemarks specified by dwOffsetLow and dwOffsetHigh in the current partition. The dwPartition parameter is ignored.</li>
     * <li>TAPE_SPACE_RELATIVE_BLOCKS (5L): Moves the tape forward (or backward) the number of blocks specified by dwOffsetLow and dwOffsetHigh in the current partition. The dwPartition parameter is ignored.</li>
     * <li>TAPE_SPACE_SEQUENTIAL_FMKS (7L): Moves the tape forward (or backward) to the first occurrence of n filemarks in the current partition, where n is the number specified by dwOffsetLow and dwOffsetHigh. The dwPartition parameter is ignored.</li>
     * <li>TAPE_SPACE_SEQUENTIAL_SMKS (9L): Moves the tape forward (or backward) to the first occurrence of n setmarks in the current partition, where n is the number specified by dwOffsetLow and dwOffsetHigh. The dwPartition parameter is ignored.</li>
     * <li>TAPE_SPACE_SETMARKS (8L): Moves the tape forward (or backward) the number of setmarks specified by dwOffsetLow and dwOffsetHigh in the current partition. The dwPartition parameter is ignored. </li>
     *                         </ul>
     * @param dwPartition Partition to position within. If dwPartition is zero, the current partition is used. Partitions are numbered logically from 1 through n, where 1 is the first partition on the tape and n is the last.
     * @param dwOffsetLow Low-order bits of the block address or count for the position operation specified by the dwPositionMethod parameter.
     * @param dwOffsetHigh High-order bits of the block address or count for the position operation specified by the dwPositionMethod parameter. If the high-order bits are not required, this parameter should be zero.
     * @param bImmediate Indicates whether to return as soon as the move operation begins. If this parameter is TRUE, the function returns immediately; if FALSE, the function does not return until the move operation has been completed.
     * @returnIf the function succeeds, the return value is NO_ERROR.
     * If the function fails, it can return one of the following error codes.<br/>
     * <ul>
     *     <li>ERROR_BEGINNING_OF_MEDIA (1102L): An attempt to access data before the beginning-of-medium marker failed.</li>
     * <li>ERROR_BUS_RESET (1111L): A reset condition was detected on the bus.</li>
     * <li>ERROR_DEVICE_NOT_PARTITIONED (1107L): The partition information could not be found when a tape was being loaded.</li>
     * <li>ERROR_END_OF_MEDIA (1100L): The end-of-tape marker was reached during an operation.</li>
     * <li>ERROR_FILEMARK_DETECTED (1101L): A filemark was reached during an operation.</li>
     * <li>ERROR_INVALID_BLOCK_LENGTH (1106L): The block size is incorrect on a new tape in a multivolume partition.</li>
     * <li>ERROR_MEDIA_CHANGED (1110L): The tape that was in the drive has been replaced or removed.</li>
     * <li>ERROR_NO_DATA_DETECTED (1104L): The end-of-data marker was reached during an operation.</li>
     * <li>ERROR_NO_MEDIA_IN_DRIVE (1112L): There is no media in the drive.</li>
     * <li>ERROR_NOT_SUPPORTED (50L): The tape driver does not support a requested function.</li>
     * <li>ERROR_PARTITION_FAILURE (1105L): The tape could not be partitioned.</li>
     * <li>ERROR_SETMARK_DETECTED (1103L): A setmark was reached during an operation.</li>
     * <li>ERROR_UNABLE_TO_LOCK_MEDIA (1108L): An attempt to lock the ejection mechanism failed.</li>
     * <li>ERROR_UNABLE_TO_UNLOAD_MEDIA (1109L): An attempt to unload the tape failed.</li>
     * <li>ERROR_WRITE_PROTECT (19L): The media is write protected.</li>
     * </ul>
     */
    public DWORD GetTapePosition(HANDLE hDevice, DWORD dwPositionMethod, DWORD dwPartition, DWORDByReference dwOffsetLow, DWORDByReference dwOffsetHigh, BOOL bImmediate);

    /**
     * The WriteTapemark function writes a specified number of filemarks, setmarks, short filemarks, or long filemarks to a tape device. These tapemarks divide a tape partition into smaller areas.
     * @param hDevice Handle to the device on which to write tapemarks. This handle is created by using the CreateFile function.
     * @param dwTapemarkType Type of tapemarks to write. This parameter can be one of the following values.<br/>
     *                       <ul>
     *                       <li>TAPE_FILEMARKS (1L): Writes the number of filemarks specified by the dwTapemarkCount parameter.</li>
     *                       <li>TAPE_LONG_FILEMARKS (3L):  Writes the number of long filemarks specified by dwTapemarkCount.</li>
     *                       <li>TAPE_SETMARKS (0L):  Writes the number of setmarks specified by dwTapemarkCount.</li>
     *                       <li>TAPE_SHORT_FILEMARKS (2L):  Writes the number of short filemarks specified by dwTapemarkCount.</li>
     *                       </ul>
     * @param dwTapemarkCount Number of tapemarks to write.
     * @param bImmediate If this parameter is TRUE, the function returns immediately; if it is FALSE, the function does not return until the operation has been completed.
     * @return If the function succeeds, the return value is NO_ERROR.
     * If the function fails, it can return one of the following error codes.<br/>
     *<ul>
     *     <li>ERROR_BEGINNING_OF_MEDIA(1102L): An attempt to access data before the beginning-of-medium marker failed.</li>
     * <li>ERROR_BUS_RESET(1111L): A reset condition was detected on the bus.</li>
     * <li>ERROR_DEVICE_NOT_PARTITIONED(1107L): The partition information could not be found when a tape was being loaded.</li>
     * <li>ERROR_END_OF_MEDIA(1100L): The end-of-tape marker was reached during an operation.</li>
     * <li>ERROR_FILEMARK_DETECTED(1101L): A filemark was reached during an operation.</li>
     * <li>ERROR_INVALID_BLOCK_LENGTH(1106L): The block size is incorrect on a new tape in a multivolume partition.</li>
     * <li>ERROR_MEDIA_CHANGED(1110L): The tape that was in the drive has been replaced or removed.</li>
     * <li>ERROR_NO_DATA_DETECTED(1104L): The end-of-data marker was reached during an operation.</li>
     * <li>ERROR_NO_MEDIA_IN_DRIVE(1112L): There is no media in the drive.</li>
     * <li>ERROR_NOT_SUPPORTED(50L): The tape driver does not support a requested function.</li>
     * <li>ERROR_PARTITION_FAILURE(1105L): The tape could not be partitioned.</li>
     * <li>ERROR_SETMARK_DETECTED(1103L): A setmark was reached during an operation.</li>
     * <li>ERROR_UNABLE_TO_LOCK_MEDIA(1108L): An attempt to lock the ejection mechanism failed.</li>
     * <li>ERROR_UNABLE_TO_UNLOAD_MEDIA(1109L): An attempt to unload the tape failed.</li>
     * <li>ERROR_WRITE_PROTECT(19L): The media is write protected. </li>
     *</ul>
     */
    public DWORD WriteTapemark(HANDLE hDevice, DWORD dwTapemarkType, DWORD dwTapemarkCount, BOOL bImmediate);

    /**
     * The GetTapeParameters function retrieves information that describes the tape or the tape drive.
     * @param hDevice Handle to the device about which information is sought. This handle is created by using the {@link #CreateFile(String, int, int, SECURITY_ATTRIBUTES, int, int, HANDLE)} function.
     * @param dwOperation Type of information requested. This parameter must be one of the following values.<br/>
     *                    <ul>
     *                    <li>GET_TAPE_DRIVE_INFORMATION (1): Retrieves information about the tape device.</li>
     *                    <li>GET_TAPE_MEDIA_INFORMATION (0):  Retrieves information about the tape in the tape device.</li>
     *                    </ul>
     * @param lpdwSize Pointer to a variable that receives the size, in bytes, of the buffer specified by the lpTapeInformation parameter. If the buffer is too small, this parameter receives the required size.
     * @param lpTapeInformation Pointer to a structure that contains the requested information. If the dwOperation parameter is GET_TAPE_MEDIA_INFORMATION, lpTapeInformation points to a TAPE_GET_MEDIA_PARAMETERS structure.<br/>
     *                   If dwOperation is GET_TAPE_DRIVE_INFORMATION, lpTapeInformation points to a TAPE_GET_DRIVE_PARAMETERS structure.
     * @return If the function succeeds, the return value is NO_ERROR.<br/>
     * If the function fails, it can return one of the following error codes.<br/>
     * <ul>
     * <li>ERROR_BEGINNING_OF_MEDIA (1102L): An attempt to access data before the beginning-of-medium marker failed.</li>
     * <li>ERROR_BUS_RESET (1111L): A reset condition was detected on the bus.</li>
     * <li>ERROR_DEVICE_NOT_PARTITIONED (1107L): The partition information could not be found when a tape was being loaded.</li>
     * <li>ERROR_END_OF_MEDIA (1100L): The end-of-tape marker was reached during an operation.</li>
     * <li>ERROR_FILEMARK_DETECTED (1101L): A filemark was reached during an operation.</li>
     * <li>ERROR_INVALID_BLOCK_LENGTH (1106L): The block size is incorrect on a new tape in a multivolume partition.</li>
     * <li>ERROR_MEDIA_CHANGED (1110L): The tape that was in the drive has been replaced or removed.</li>
     * <li>ERROR_NO_DATA_DETECTED (1104L): The end-of-data marker was reached during an operation.</li>
     * <li>ERROR_NO_MEDIA_IN_DRIVE (1112L): There is no media in the drive.</li>
     * <li>ERROR_NOT_SUPPORTED (50L): The tape driver does not support a requested function.</li>
     * <li>ERROR_PARTITION_FAILURE (1105L): The tape could not be partitioned.</li>
     * <li>ERROR_SETMARK_DETECTED (1103L): A setmark was reached during an operation.</li>
     * <li>ERROR_UNABLE_TO_LOCK_MEDIA (1108L): An attempt to lock the ejection mechanism failed.</li>
     * <li>ERROR_UNABLE_TO_UNLOAD_MEDIA (1109L): An attempt to unload the tape failed.</li>
     * <li>ERROR_WRITE_PROTECT (19L): The media is write protected. </li>
     * </ul>
     */
    public DWORD GetTapeParameters(HANDLE hDevice, DWORD dwOperation, DWORDByReference lpdwSize, Object lpTapeInformation);

    /**
     * The SetTapeParameters function either specifies the block size of a tape or configures the tape device.
     * @param hDevice Handle to the device for which to set configuration information. This handle is created by using the {@link #CreateFile(String, int, int, SECURITY_ATTRIBUTES, int, int, HANDLE)} function.
     * @param dwOperation Type of information to set. This parameter must be one of the following values.<br/>
     *                    <ul>
     *                    <li>SET_TAPE_DRIVE_INFORMATION (1): Sets the device-specific information specified by lpTapeInformation.</li>
     *                    <li>SET_TAPE_MEDIA_INFORMATION (0): Sets the tape-specific information specified by the lpTapeInformation parameter.</li>
     *                    </ul>
     * @param lpTapeInformation Pointer to a structure that contains the information to set. If the dwOperation parameter is SET_TAPE_MEDIA_INFORMATION, lpTapeInformation points to a TAPE_SET_MEDIA_PARAMETERS structure.<br/>If dwOperation is SET_TAPE_DRIVE_INFORMATION, lpTapeInformation points to a TAPE_SET_DRIVE_PARAMETERS structure.
     * @return If the function succeeds, the return value is NO_ERROR.<br/>
     * If the function fails, it can return one of the following error codes.<br/>
     * <ul>
     *     <li>ERROR_BEGINNING_OF_MEDIA (1102L)</li>
     *     <li>ERROR_BUS_RESET (1111L)</li>
     *     <li>ERROR_DEVICE_NOT_PARTITIONED (1107L)</li>
     *     <li>ERROR_END_OF_MEDIA (1100L)</li>
     *     <li>ERROR_FILEMARK_DETECTED (1101L)</li>
     *     <li>ERROR_INVALID_BLOCK_LENGTH (1106L)</li>
     *     <li>ERROR_MEDIA_CHANGED (1110L)</li>
     *     <li>ERROR_NO_DATA_DETECTED (1104L)</li>
     *     <li>ERROR_NO_MEDIA_IN_DRIVE (1112L)</li>
     *     <li>ERROR_NOT_SUPPORTED (50L)</li>
     *     <li>ERROR_PARTITION_FAILURE (1105L)</li>
     *     <li>ERROR_SETMARK_DETECTED (1103L)</li>
     *     <li>ERROR_UNABLE_TO_LOCK_MEDIA (1108L)</li>
     *     <li>ERROR_UNABLE_TO_UNLOAD_MEDIA (1109L)</li>
     *     <li>ERROR_WRITE_PROTECT (19L)</li>
     * </ul>
     */
    public DWORD SetTapeParameters(HANDLE hDevice, DWORD dwOperation, Object lpTapeInformation);

    /**
     * The GetTapeStatus function determines whether the tape device is ready to process tape commands.
     * @param hDevice
     * @return If the tape device is ready to accept appropriate tape-access commands without returning errors, the return value is NO_ERROR.<br/>
     * If the function fails, it can return one of the following error codes.<br/>
     * <ul>
     * <li>ERROR_BEGINNING_OF_MEDIA (1102L): An attempt to access data before the beginning-of-medium marker failed.</li>
     * <li>ERROR_BUS_RESET (1111L): A reset condition was detected on the bus.</li>
     * <li>ERROR_DEVICE_NOT_PARTITIONED (1107L): The partition information could not be found when a tape was being loaded.</li>
     * <li>ERROR_DEVICE_REQUIRES_CLEANING (1165L): The tape drive is capable of reporting that it requires cleaning, and reports that it does require cleaning.</li>
     * <li>ERROR_END_OF_MEDIA (1100L): The end-of-tape marker was reached during an operation.</li>
     * <li>ERROR_FILEMARK_DETECTED (1101L): A filemark was reached during an operation.</li>
     * <li>ERROR_INVALID_BLOCK_LENGTH (1106L): The block size is incorrect on a new tape in a multivolume partition.</li>
     * <li>ERROR_MEDIA_CHANGED (1110L): The tape that was in the drive has been replaced or removed.</li>
     * <li>ERROR_NO_DATA_DETECTED (1104L): The end-of-data marker was reached during an operation.</li>
     * <li>ERROR_NO_MEDIA_IN_DRIVE (1112L): There is no media in the drive.</li>
     * <li>ERROR_NOT_SUPPORTED (50L): The tape driver does not support a requested function.</li>
     * <li>ERROR_PARTITION_FAILURE (1105L): The tape could not be partitioned.</li>
     * <li>ERROR_SETMARK_DETECTED (1103L): A setmark was reached during an operation.</li>
     * <li>ERROR_UNABLE_TO_LOCK_MEDIA (1108L): An attempt to lock the ejection mechanism failed.</li>
     * <li>ERROR_UNABLE_TO_UNLOAD_MEDIA (1109L): An attempt to unload the tape failed.</li>
     * <li>ERROR_WRITE_PROTECT (19L): The media is write protected.</li>
     * </ul>
     */
    public DWORD GetTapeStatus(HANDLE hDevice);

    /**
     * The CreateTapePartition function reformats a tape.
     * @param hDevice Handle to the device where the new partition is to be created. This handle is created by using the CreateFile function.
     * @param dwPartitionMethod Type of partition to create. To determine what type of partitions your device supports, see the documentation for your hardware. This parameter can have one of the following values:
     * <ul>
     * <li>TAPE_FIXED_PARTITIONS (0L): Partitions the tape based on the device's default definition of partitions. The dwCount and dwSize parameters are ignored.</li>
     * <li>TAPE_SELECT_PARTITIONS (1L):  Partitions the tape into the number and size of partitions specified by dwCount and dwSize, respectively, except for the last partition. The size of the last partition is the remainder of the tape. </li>
     * <li>TAPE_INITIATOR_PARTITIONS (2L):  Partitions the tape into the number of partitions specified by dwCount. The dwSize parameter is ignored. The size of the partitions is determined by the device's default partition size. For more specific information, see the documentation for your tape device. </li>
     * </ul>
     * @param dwCount Number of partitions to create. The GetTapeParameters function provides the maximum number of partitions a tape can support.
     * @param dwSize Size of each partition, in megabytes. This value is ignored if the dwPartitionMethod parameter is TAPE_SELECT_PARTITIONS.
     * @return If the function succeeds, the return value is NO_ERROR.
     *
     * If the function fails, it can return one of the following error codes. (TODO [Low])
     */
    public DWORD CreateTapePartition(HANDLE hDevice, DWORD dwPartitionMethod, DWORD dwCount, DWORD dwSize);
}
