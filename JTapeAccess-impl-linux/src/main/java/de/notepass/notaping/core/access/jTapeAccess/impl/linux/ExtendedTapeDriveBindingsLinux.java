package de.notepass.notaping.core.access.jTapeAccess.impl.linux;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import de.notepass.notaping.core.access.jTapeAccess.model.*;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;

import static de.notepass.notaping.core.access.jTapeAccess.impl.linux.LibcWithFileOP.*;

public class ExtendedTapeDriveBindingsLinux implements ExtendedTapeDrive {
    private final LibcWithFileOP libc = LibcWithFileOP.INSTANCE;
    private LibcWithFileOP.FILE handle;
    private int fileDescriptor;
    private String tapeDriveFile;
    private boolean readOnlyMode = false;
    private static Logger LOGGER = LoggerFactory.getLogger(ExtendedTapeDriveBindingsLinux.class);
    private boolean addressModeSwitched = false;

    public static final OS TARGET_OS = OS.LINUX;

    public void switchToLogicalAlternativeAddressMode() throws TapeException {
        if (!addressModeSwitched) {
            LOGGER.debug("Requesting driver to switch to alternative block addressing mode");

            // I never found any tape devices that work with the standard block addressing mode
            execSimpleCommand(IOCTL_COMMAND_SET_OPTION, IOCTL_REQUEST_MT_ALTERNATIVE_BLOCK_MODE);

            addressModeSwitched = true;
        }
    }

    @Override
    public void loadTape() throws TapeException {
        execSimpleCommand(IOCTL_COMMAND_COMMAND_LOAD_TAPE, 1);
    }

    @Override
    public void ejectTape() throws TapeException {
        execSimpleCommand(IOCTL_COMMAND_COMMAND_EJECT_TAPE, 1);
    }

    @Override
    public void setTapeMetadataListener(TapeMetadataEncounteredListener listener) {

    }

    @Override
    public OS getTargetOs() {
        return OS.LINUX;
    }

    @Override
    public boolean isReadable() throws TapeException {
        return getTapeDriveStatus().isTapePresent();
    }

    @Override
    public boolean isWritable() throws TapeException {
        return !getTapeDriveStatus().isWriteProtected() && !readOnlyMode;
    }

    @Override
    public long getTapeBlockPosition() throws TapeException {
        return execGetPositionCommand();
    }

    @Override
    public void setTapeBlockPosition(long pos) throws TapeException {
        switchToLogicalAlternativeAddressMode();
        LOGGER.debug("Requesting tape device to skip to block "+pos);
        execSimpleCommand(IOCTL_COMMAND_COMMAND_SEEK, (int) pos);
    }

    @Override
    public void forwardFilemarks(int amount) throws TapeException {
        execSimpleCommand(IOCTL_COMMAND_FORWARD_FILEMARK, amount);
    }

    @Override
    public void reverseFilemarks(int amount) throws TapeException {
        execSimpleCommand(IOCTL_COMMAND_REVERSE_FILEMARK, amount);
    }

    @Override
    public void writeFilemarks(int amount) throws TapeException {
        execSimpleCommand(IOCTL_COMMAND_WRITE_FILEMARK, amount);
    }

    @Override
    public DriveCapabilities[] getDriveCapabilities() throws TapeException {
        //TODO: [High] Where to fetch this from in linux?
        return new DriveCapabilities[]{DriveCapabilities.COMPRESSION};
    }

    @Override
    public void enableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {
        //TODO: [High] Currently only compression support under linux via specific code path
        if (Arrays.stream(driveCapabilities).anyMatch(c -> c == DriveCapabilities.COMPRESSION)) {
            execSimpleCommand(IOCTL_COMMAND_COMMAND_COMPRESSION, 1);
        }
    }

    @Override
    public void disableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {
        //TODO: [High] Currently only compression support under linux via specific code path
        if (Arrays.stream(driveCapabilities).anyMatch(c -> c == DriveCapabilities.COMPRESSION)) {
            execSimpleCommand(IOCTL_COMMAND_COMMAND_COMPRESSION, 0);
        }
    }

    @Override
    public void setMediumBlockSize(int blockSizeBytes) throws TapeException {
        execSimpleCommand(IOCTL_COMMAND_COMMAND_SET_BLOCK_SIZE, blockSizeBytes);
    }

    @Override
    public int readBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        byte[] sub = new byte[numBytes];
        int read = readBytes(sub);
        System.arraycopy(sub, 0, buffer, offset, read);
        return read;
    }

    @Override
    public int writeBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        if (numBytes == 0) {
            return 0;
        }

        byte[] cp = new byte[numBytes];
        System.arraycopy(buffer, offset, cp, 0, numBytes);
        return writeBytes(cp);
    }

    @Override
    public int readBytes(byte[] buffer) throws TapeException {
        return checkNativeError((int) libc.read(fileDescriptor, buffer, buffer.length));
    }

    @Override
    public int writeBytes(byte[] buffer) throws TapeException {
        /*
        try {
            Files.write(Path.of("/tmp/debug.bin"), buffer, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }

         */

        if (buffer.length == 0) {
            return 0;
        }

        //Pointer buffPtr = new Memory(buffer.length);
        //buffPtr.write(0, buffer, 0, buffer.length);

        // For some reason, fwrite will break the write calls, so using write call directly
        // See: https://stackoverflow.com/questions/67338260
        int written = checkNativeError((int) libc.write(fileDescriptor, buffer, buffer.length));
        //Native.free(Pointer.nativeValue(buffPtr));
        return written;
    }

    private BlockSizeCorrectingTapeOutputStream<WrittenBytesAwareOutputStream> outputStream;
    @Override
    public WrittenBytesAwareOutputStream getOutputStream() throws TapeException {
        if (outputStream == null) {
            outputStream = new BlockSizeCorrectingTapeOutputStream<>(getRawOutputStream(), (int) getTapeInfo().getBlockSizeBytes());
        }

        return outputStream;
    }

    private BlockSizeCorrectingTapeInputStream<TapeInputStream> inputStream;
    @Override
    public InputStream getInputStream() throws TapeException {
        if (inputStream == null) {
            inputStream = new BlockSizeCorrectingTapeInputStream<>((TapeInputStream) getRawInputStream());
        }

        return inputStream;
    }

    private WrittenBytesAwareOutputStream rawOutputStream;
    @Override
    public WrittenBytesAwareOutputStream getRawOutputStream() throws TapeException {
        if (rawOutputStream == null) {
            rawOutputStream = new TapeOutputStream(this);
        }

        return rawOutputStream;
    }

    private TapeInputStream rawInputStream;
    @Override
    public InputStream getRawInputStream() throws TapeException {
        if (rawInputStream == null) {
            rawInputStream = new TapeInputStream(this);
        }

        return rawInputStream;
    }

    @Override
    public void flushCaches() throws TapeException {
        // According to mt-st this should flush all caches
        execSimpleCommand(IOCTL_COMMAND_NOOP, 0);
    }

    @Override
    public boolean isMediaPresent() throws TapeException {
        return getTapeDriveStatus().isTapePresent();
    }

    @Override
    public TapeInfo getTapeInfo() throws TapeException {
        LibcWithFileOP.IOCTL_MT_STATUS_INFO status = getTapeDriveStatus();
        return new TapeInfo(-1, -1, -1, status.isWriteProtected(), status.getBlockSize());
    }

    @Override
    public void setTapeDriveFile(String tapeDriveFile) throws TapeException {
        if (fileDescriptor > 0) {
            if (tapeDriveFile.equals(this.tapeDriveFile)) {
                // Tried to initialize to the same device as currently assigned to. Can be ignored
                return;
            }
            throw new TapeException(TapeException.ErrorCode.CLIENT_REINITIALIZATION_NOT_SUPPORTED, "This client cannot switch tape devices!");
        }

        // TODO: [Low] Put into utility method
        char[] pathChars = tapeDriveFile.toCharArray();
        byte[] pathBytes = new byte[pathChars.length + 1];
        for (int i = 0; i < pathChars.length; i++) {
            pathBytes[i] = (byte) pathChars[i];
        }
        pathBytes[pathBytes.length - 1] = (byte) '\0';
        Pointer pathPtr = new Memory(pathBytes.length);
        pathPtr.write(0, pathBytes, 0, pathBytes.length);

        // Open in read-only mode to access metadata
        if(libc.access(pathPtr, MODE_R_OK) == 0) {
            fileDescriptor = libc.open(pathPtr, ACCESS_LEVEL_RO);
        } else {
            throw new TapeException(TapeException.ErrorCode.DEVICE_NOT_FOUND, "Cannot access path "+tapeDriveFile+". Does the user has permission to access it?");
        }
        IOCTL_MT_STATUS_INFO tapeDeviceStatus = getTapeDriveStatus();
        // Check for write protection on the tape media and access permission to the tape device
        if (!tapeDeviceStatus.isWriteProtected() && libc.access(pathPtr, MODE_R_OK | MODE_W_OK) == 0) {
            // Reopen with RW access
            libc.close(fileDescriptor);
            fileDescriptor = libc.open(pathPtr, ACCESS_LEVEL_RW);
        }

        if (fileDescriptor <= 0) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_NOT_FOUND, "Could not open tape drive " + tapeDriveFile + ". Is it connected and is the application running with administrator privileges?");
        }
        // Make sure that the tape handle is closed upon exit
        Runtime.getRuntime().addShutdownHook(new Thread(() -> libc.close(fileDescriptor)));

        this.tapeDriveFile = tapeDriveFile;
    }

    @Override
    public String getTapeDriveFile() {
        return tapeDriveFile;
    }

    private LibcWithFileOP.IOCTL_MT_STATUS_INFO getTapeDriveStatus() throws TapeException {
        LibcWithFileOP.IOCTL_MT_STATUS_INFO.ByReference status = new LibcWithFileOP.IOCTL_MT_STATUS_INFO.ByReference();
        checkNativeError(libc.ioctl(fileDescriptor, IOCTL_REQUEST_MT_GET_STATUS, status));
        return status;
    }

    private int execSimpleCommand(short op, int amount) throws TapeException {
        IOCTL_MT_SIMPLE_COMMAND.ByReference command = new IOCTL_MT_SIMPLE_COMMAND.ByReference();
        command.command = op;
        command.count = amount;
        return checkNativeError(libc.ioctl(fileDescriptor, IOCTL_REQUEST_MT_SIMPLE_OP, command));
    }

    private long execGetPositionCommand() throws TapeException {
        switchToLogicalAlternativeAddressMode();
        IOCTL_MT_POSITION_INFO.ByReference pos = new IOCTL_MT_POSITION_INFO.ByReference();
        checkNativeError(libc.ioctl(fileDescriptor, IOCTL_REQUEST_MT_GET_POSITION_INFO, pos));
        return pos.mt_blkno;
    }

    private int checkNativeError(int result) throws TapeException {
        if (result < 0) {
            int errCode = Native.getLastError();
            String errorMsg = libc.strerror(tapeDriveFile);
            // TODO: [Blocker] Proper mapping
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Native call returned error: "+errorMsg+" (Code: "+errCode+")");
        }

        return result;
    }
}
