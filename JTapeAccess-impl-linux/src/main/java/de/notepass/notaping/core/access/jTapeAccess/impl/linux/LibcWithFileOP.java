package de.notepass.notaping.core.access.jTapeAccess.impl.linux;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.PointerType;
import com.sun.jna.Structure;
import com.sun.jna.platform.unix.LibC;

import java.util.Arrays;
import java.util.List;

public interface LibcWithFileOP extends LibC {
    LibcWithFileOP INSTANCE = Native.load(NAME, LibcWithFileOP.class);
    String MODE_READ_AND_WRITE = "r+";

    int MODE_R_OK = 4;
    int MODE_W_OK = 2;
    int MODE_X_OK = 1;
    int MODE_F_OK = 0;

    int ACCESS_LEVEL_RO = 0; //Only read access
    int ACCESS_LEVEL_WO = 1; //Only write access
    int ACCESS_LEVEL_RW = 2; //R+W access

    long IOCTL_REQUEST_MT_GET_STATUS = 2150657282L;
    long IOCTL_REQUEST_MT_SIMPLE_OP = 1074294017L;
    long IOCTL_REQUEST_MT_GET_POSITION_INFO = 2148035843L;

    public static final short IOCTL_COMMAND_FORWARD_FILEMARK = 1;
    public static final short IOCTL_COMMAND_WRITE_FILEMARK = 5;
    public static final short IOCTL_COMMAND_NOOP = 8; // Flushes cache
    public static final short IOCTL_COMMAND_REVERSE_FILEMARK = 10; //Positions tape at BEGGING OF NEXT FILE; 2 would position at last block of given file)
    public static final short IOCTL_COMMAND_COMMAND_SET_BLOCK_SIZE = 20;
    public static final short IOCTL_COMMAND_COMMAND_SEEK = 22;
    public static final short IOCTL_COMMAND_SET_OPTION = 24;
    public static final short IOCTL_COMMAND_COMMAND_LOAD_TAPE = 30;
    public static final short IOCTL_COMMAND_COMMAND_EJECT_TAPE = 31;
    public static final short IOCTL_COMMAND_COMMAND_COMPRESSION = 32;
    public static final short IOCTL_COMMAND_SET_PARTITION = 33;
    public static final int IOCTL_REQUEST_MT_ALTERNATIVE_BLOCK_MODE = 268437504;

    public class FILE extends PointerType {

    }

    int access(Pointer path, int mode);
    int open(Pointer path, int flags);
    int close(int fd);
    long write(int fd, Pointer data, long bytesToWrite);
    long write(int fd, byte[] data, long bytesToWrite);
    long read(int fd, byte[] data, long bytesToWrite);
    long read(int fd, Pointer data, long bytesToWrite);

    int fileno(FILE handle);

    int ioctl(int fd, long request, Object... args);

    void perror(String prefix);

    String strerror(String prefix);

    public static class IOCTL_MT_SIMPLE_COMMAND extends Structure {
        public static class ByReference extends IOCTL_MT_SIMPLE_COMMAND implements Structure.ByReference {

        };

        public static class ByValue extends IOCTL_MT_SIMPLE_COMMAND implements Structure.ByValue {

        };

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList(
                    "command",
                    "count"
            );
        }

        public short command;
        public int count;
    }

    public static class IOCTL_MT_POSITION_INFO extends Structure {
        public static class ByReference extends IOCTL_MT_POSITION_INFO implements Structure.ByReference {

        };

        public static class ByValue extends IOCTL_MT_POSITION_INFO implements Structure.ByValue {

        };

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList(
                    "mt_blkno"
            );
        }

        public long mt_blkno;
    }

    public static class IOCTL_MT_STATUS_INFO extends Structure {
        public static class ByReference extends IOCTL_MT_STATUS_INFO implements Structure.ByReference {

        };

        public static class ByValue extends IOCTL_MT_STATUS_INFO implements Structure.ByValue {

        };

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList(
                    "tapeType",
                    "something",
                    "deviceStatus1",
                    "deviceStatus2",
                    "errorInfo",
                    "filesOnTape",
                    "currentBlockIndex"
            );
        }

        public long tapeType;
        public long something;
        public long deviceStatus1;
        public long deviceStatus2;
        public long errorInfo;
        public int filesOnTape;
        public int currentBlockIndex;

        public boolean isTapePresent() {
            // 0x00040000 true = no tape present / drive ready to accept a tape
            return (deviceStatus2 & 0x00040000) == 0;
        }

        public int getBlockSize() {
            return (int) (deviceStatus1 & 0xffffff);
        }

        public boolean isWriteProtected() {
            return (deviceStatus2 & 0x04000000) > 0;
        }
    }
}
