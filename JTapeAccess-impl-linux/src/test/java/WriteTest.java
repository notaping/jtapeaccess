import de.notepass.notaping.core.access.jTapeAccess.impl.linux.ExtendedTapeDriveBindingsLinux;
import de.notepass.notaping.core.access.jTapeAccess.model.TapeException;

import java.util.Arrays;

public class WriteTest {
    public static void main(String[] args) throws TapeException {
        ExtendedTapeDriveBindingsLinux t = new ExtendedTapeDriveBindingsLinux();
        t.setTapeDriveFile("/root/test.txt");
        byte[] data = new byte[64 * 1024];
        Arrays.fill(data, (byte) '9');
        t.writeBytes(data);
    }
}
